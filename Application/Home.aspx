﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div>
        <h1>Claims System</h1>
        <p>University of Technology, Jamaica Overtime Claims Application</p>
         <p>
            <a class="btn black" runat="server" href="~/Account/Register.aspx">Create Account</a>
            <a class="btn black" runat="server" href="~/Account/Login.aspx">Sign in</a>
        </p>
    </div>
</asp:Content>