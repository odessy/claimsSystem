﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

public partial class UsersIndex : System.Web.UI.Page
{

    private void BindList(MembershipUserCollection membershipUserCollection)
    {
        gvUsers.DataSource = membershipUserCollection;
        gvUsers.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindList(Membership.GetAllUsers());
     
        }
    }

    protected void checkBoxRepeater_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        }
    }

    protected void roleMembershipChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox box = (CheckBox)sender;
            string role = box.Text;
            string currentUser = lbAddToRole.ToolTip;

            currentUser = UserAccount.getUserInfoByEmail(currentUser).idNumber;

            if (box.Checked)
            {
                Roles.AddUserToRole(currentUser, role);
            }
            else
            {
               string [] list = Roles.GetRolesForUser(currentUser);
               if (list.Length > 1)
                   Roles.RemoveUserFromRole(currentUser, role);
               else
               {
                   box.Checked = true;
                   throw new Exception("User must have atleast one role");
               }
            }
        }
        catch(Exception ex)
        {
            // Ignore, e.g., user is already in role.
            CustomUtility.standardErrorHandling(ex);
        }
    }

    protected bool isUserInRole(string role)
    {
        UserAccount user = UserAccount.getUserInfoByEmail(lbAddToRole.ToolTip);
        if (user == null) return false;
        String [] list = Roles.FindUsersInRole(role, user.idNumber);
        return list.Length != 0;
    }

    protected bool isUserLoggedIn(string email)
    {
        UserAccount user = UserAccount.getUserInfoByEmail(email);
        if (user == null) return false;
        MembershipUser membershipUser = Membership.GetUser(user.idNumber);
        return membershipUser.IsOnline;
    }

    protected void buttonClick(object sender, EventArgs e)
    {
        LinkButton button = (LinkButton)sender;
        string userEmail = button.CommandArgument;

        lbAddToRole.ToolTip = userEmail;

        lbAddToRole.Text = "Add Roles To User " + userEmail;

        checkBoxRepeater.DataSource = Roles.GetAllRoles();
        checkBoxRepeater.DataBind();
        //rolePlaceHolder.Visible = true;
    }
    protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvUsers.PageIndex = e.NewPageIndex;
        BindList(Membership.GetAllUsers());
    }
}