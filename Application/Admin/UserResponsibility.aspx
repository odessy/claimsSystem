﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserResponsibility.aspx.cs" Inherits="Admin_UserResponsibility" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="fuserResponsibility" class="form-horizontal" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h3 class="modal-title" id="myModalLabel">Add Responsibility</h3>
        </div>
        <div class="modal-body form">
            <div id="flash"></div>
            <div class="form">
                <asp:TextBox ID="tbUserId" hidden="true" runat="server"></asp:TextBox>
                <asp:CheckBoxList CssClass="cblResponsibility" ID="cbResponsibilities" RepeatColumns="2" runat="server"></asp:CheckBoxList>
            </div>
        </div>
        <div class="modal-footer">
            <div class="form-group">
                <button type="button" class="btn btn-default" id="cancelButton" data-dismiss="modal" aria-hidden="true">Cancel</button>
                <asp:Button PostBackUrl="~/Admin/UserResponsibility.aspx" ID="btnSubmit" CausesValidation="false" CssClass="btn btn-primary"
                    runat="server" Text="Submit" />
            </div>
        </div>
    </form>
    <script type="text/javascript">

        $('#fuserResponsibility').submit(function () {

            $.post('/Admin/UserResponsibility', $('#fuserResponsibility').serialize(), function (data) {
                //call back happens here.  Unblock form/show result
                //$("#flash").html("This is the response message");
                window.location = window.location;
            });

            return false; // this prevents regular post, forcing ajax post.
        });
    </script>
</body>
</html>