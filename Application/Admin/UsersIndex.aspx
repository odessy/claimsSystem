﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UsersIndex.aspx.cs" Inherits="UsersIndex" %>

<%@ Register Assembly="DataPagerGridView" Namespace="ApplicationControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Manage Users</h1></asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterScript" runat="server">
    <script type="text/javascript">

        function showModal(user) {
            GetData(user);
        }

        function GetData(user) {
            $.ajax({
                method: "GET",
                url: '/Admin/UserResponsibility?user='+user,
                dataType: 'text',
                async: false,
                error: function (xhr, ajaxOptions, thrownError) {
                    //console.log(xhr.status);
                    //console.log(thrownError);
                },
                success: function (data) {
                    $('#repModalContent').html(data);
                    $('#repModal').modal('show');
                }
            });
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div class="alert alert-help prepend-top-default">Manage users</div>
        <div style="margin-top: 20px; margin-bottom:20px;">
            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Admin/UsersView.aspx" runat="server" Text="Insert New User" CssClass="btn black"></asp:HyperLink>
        </div>
        <div  style="margin-bottom:5px;" >
            <asp:DataPager ID="DataPager1" runat="server" PagedControlID="gvUsers"
                PageSize="6">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Button" />
                </Fields>
            </asp:DataPager>
        </div>

        <table border="0">
            <tbody>
                <tr>
                    <td>
                        <cc1:DataPagerGridView ID="gvUsers" AllowPaging="True" OnPageIndexChanging="gvUsers_PageIndexChanging" runat="server" AutoGenerateColumns="false" DataKeyNames="ProviderUserKey" CssClass="gridTable">
                            <Columns>
                                <asp:BoundField DataField="userName" HeaderText="ID Number" SortExpression="userName" />
                                <asp:BoundField DataField="email" HeaderText="Email" SortExpression="email" />
                                <asp:TemplateField HeaderText="loggedIn">
                                    <ItemTemplate>
                                        <asp:CheckBox Enabled="false" runat="server" Checked='<%# (bool)isUserLoggedIn(Eval("email").ToString()) %>' ID="cbLoggedIn"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:HyperLinkField HeaderText="Action(s)" Text="Edit User" DataNavigateUrlFields="ProviderUserKey"
                                    DataNavigateUrlFormatString="~/Admin/UsersView.aspx?userId={0}" />
                                <asp:TemplateField HeaderText="Edit Responsibility(s)">
                                    <ItemTemplate>
                                        <button type="button" id="btnRepModal" runat="server" onclick='<%# "showModal(\"" + Eval("ProviderUserKey") + "\");" %>' class="btn btn-primary" >Edit Responsibility</button>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit Role(s)">
                                    <ItemTemplate>
                                        <asp:LinkButton PostBackUrl="~/Admin/UsersIndex" runat="server" CommandArgument='<%# Eval("email")%>' Text="Edit Role" OnClick="buttonClick" ID="btnClick"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </cc1:DataPagerGridView>
                    </td>
                    <td style="width:auto; height:100%; padding-left:10px;  background-color: none;">
                        <asp:PlaceHolder runat="server" ID="rolePlaceHolder">
                            <table>
                                <tr>
                                    <td class="lrbBorders">
                                        <asp:Label runat="server" ID="lbAddToRole" Text="Add Roles To User No User Selected" /><br />
                                        <asp:Repeater runat="server" ID="checkBoxRepeater" OnItemDataBound="checkBoxRepeater_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="cbRole" AutoPostBack="true" OnCheckedChanged="roleMembershipChanged" Text='<%# Container.DataItem.ToString() %>' Checked='<%# (bool)isUserInRole(Container.DataItem.ToString()) %>' />
                                                <br />
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <style type="text/css">
        .cblResponsibility label {
            margin-left: 5px;
            margin-right: 5px;
        }
     </style>

    <!-- Modal -->
    <div id="repModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="repModalLabel">
        <div class="modal-dialog" role="document">
            <div id="repModalContent" class="modal-content"></div>
            <!--/.modal-content-->
        </div>
    </div>
</asp:Content>