﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RoleIndex.aspx.cs" Inherits="RoleIndex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Roles</h1></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div class="alert alert-help prepend-top-default">Manage Roles</div>
        <div>
            <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvRole"
                PageSize="10" OnPreRender="DataPager1_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Button" />
                </Fields>
            </asp:DataPager>
        </div>
        <asp:ListView ID="lvRole" runat="server"
            DataKeyNames="name"
            OnItemInserted="lvRole_ItemInserted"
            OnItemDeleted="lvRole_ItemDeleted"
            OnItemUpdated="lvRole_ItemUpdated"
            OnItemUpdating="lvRole_ItemUpdating"
            OnItemInserting="lvRole_ItemInserting"
            OnItemDeleting="lvRole_ItemDeleting"
            OnItemEditing="lvRole_ItemEditing"
            InsertItemPosition="LastItem">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th id="Th2" runat="server">RoleId</th>
                        <th id="Th3" runat="server">Name</th>
                        <th id="Th1" runat="server">Action(s)</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbRoleId" runat="server" Text='<%# Bind("name") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbName" runat="server" Text='<%# Bind("name") %>' />
                    </td>
                    <td>
                        <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete" />
                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </td>
                </tr>
            </ItemTemplate>
            <EditItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbRoleId" runat="server" Text='<%# Bind("name") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="tbName" runat="server" Text='<%# Bind("name") %>' />
                        <asp:RequiredFieldValidator
                            ID="rfvEditTbName" runat="server"
                            ControlToValidate="tbName"
                            ErrorMessage="Name is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                        <asp:LinkButton ID="CancelButton" runat="server" CausesValidation="false" CommandName="Cancel" Text="Cancel" />
                    </td>
                </tr>

            </EditItemTemplate>
            <InsertItemTemplate>
                <tr style="">
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="tbName" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Name"
                            Text=' <%# Bind("name") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbName" runat="server"
                            ControlToValidate="tbName"
                            ErrorMessage="Name is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:LinkButton ID="InsertButton" runat="server" ValidationGroup="insert" CommandName="Insert" Text="Insert" />
                        <asp:LinkButton ID="CancelButton" runat="server" CausesValidation="false" CommandName="Cancel" Text="Clear" />
                    </td>
                </tr>
            </InsertItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>
