﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="User_Home" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <link href="<%: Request.ApplicationPath %>App_Themes/Default/Styles/datepicker.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Admin Home Page</h1></asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterScript" runat="server">
    <script src="<%: Request.ApplicationPath %>App_Themes/Default/Scripts/bootstrap-datepicker.js" type="text/javascript"></script>
    <script type="text/ecmascript">
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var from = $('#<%= tbFrom.ClientID %>').datepicker({
            onRender: function (date) {
                return date.valueOf() > now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            //if (ev.date.valueOf() > to.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setMonth(newDate.getMonth() + 1);
            to.setValue(newDate);
            //}
            from.hide();
            $('#<%= tbTo.ClientID %>')[0].focus();
        }).data('datepicker');

        var to = $('#<%= tbTo.ClientID %>').datepicker({
            onRender: function (date) {
                return date.valueOf() < from.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function (ev) {
            to.hide();
        }).data('datepicker');

        $(".date .input-group-addon").on("click", function () {
            $(this).prev().datepicker("show");
            $(this).prev().attr("readonly", "");
        });

        function checkState(element) {
            if ($(element).is(':checked')) {
                $("#nonManual").hide();
                $("#manual").css({ "display": "inline-block", "width": "540px" });
            }
            else {
                $("#manual").hide();
                $("#nonManual").css({ "display": "inline-block", "width": "540px" });
            }
        }

        $('#<%= cbManual.ClientID %>').change(function(){
            checkState(this);
        });

        $(document).ready(function () {
            checkState('#<%= cbManual.ClientID %>');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div class="alert alert-help prepend-top-default">Process claims</div>
        <div class="form-inline" style="margin-top: 20px; margin-bottom: 20px;">
            <div id="manual" style="display:none;">
                Period
                <asp:Label ID="lbFrom" runat="server">From: </asp:Label>
                <div id="divFrom" class="col-sm-4 input-group date">
                    <asp:TextBox ID="tbFrom" CssClass="form-control" ReadOnly="false" placeholder="mm/dd/yyyy" Text="" runat="server"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <asp:Label ID="lbTo" runat="server">To: </asp:Label>
                <div id="divTo" class="col-sm-4 input-group date">
                    <asp:TextBox ID="tbTo" CssClass="form-control" ReadOnly="false" placeholder="mm/dd/yyyy" Text="" runat="server"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div style="display:inline-block;" id="nonManual">
                Semester <asp:DropDownList ID="ddlSemester" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" runat="server"></asp:DropDownList>
                Period <asp:DropDownList ID="ddlPeriod" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
                <asp:Button ID="btnFilterClaim" runat="server" Text="Filter Claims" OnClick="btnFilterClaim_Click" CssClass="btn btn-primary"></asp:Button>
            <asp:CheckBox ID="cbManual"  CssClass="form-control" Text="&nbsp Manual" Checked="false" runat="server" />
        </div>
        <div style="margin-bottom:5px;">
            <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvClaim"
                PageSize="10" OnPreRender="DataPager1_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Button" />
                </Fields>
            </asp:DataPager>
        </div>
        <asp:ListView ID="lvClaim" runat="server"
            DataKeyNames="Id">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th id="Th2" runat="server">ClaimId</th>
                        <th id="Th5" runat="server">Semester</th>
                        <th id="Th3" runat="server">Total Hours</th>
                        <th id="Th6" runat="server">Total Claim</th>
                        <th id="Th8" runat="server">From Period</th>
                        <th id="Th9" runat="server">To Period</th>
<%--                        <th id="Th6" runat="server">Date Created</th>
                        <th id="Th7" runat="server">Date Modified</th>--%>
                        <th id="Th4" runat="server">Status</th>
                        <th id="Th1" runat="server">Action(s)</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbClaimId" runat="server" Text='<%# Eval("Id") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbSemester" runat="server" Text='<%# getSemesterDate(Eval("FK_Semester_Id").ToString()) %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotaHours" runat="server" Text='<%# Convert.ToInt32(Eval("Total_Minutes")) / 3600 %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotalClaim" runat="server" Text='<%# Eval("Total_Claim") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbPeriodFrom" runat="server" Text='<%# ((DateTime)Eval("Period_From")).ToString("MM/dd/yyyy") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbPeriodTo" runat="server" Text='<%# ((DateTime)Eval("Period_To")).ToString("MM/dd/yyyy") %>' />
                    </td>
<%--                <td>
                        <asp:Label ID="lbDateCreated" runat="server" Text='<%# Eval("Date_Created") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbDateModified" runat="server" Text='<%# Eval("Date_Modified") %>' />
                    </td>--%>
                    <td>
                        <asp:Label ID="lbStatus" runat="server" style='<%# "color:" + ClaimDB.getStringStatusClass(Convert.ToInt32(Eval("Status"))) + ";"  %>' Text='<%# ClaimDB.getStringStatus(Convert.ToInt32(Eval("Status")))  %>' />
                    </td>
                    <td>
                        <asp:LinkButton ID="ViewButton" CssClass="btn btn-info" runat="server" CommandArgument='<%# Bind("Id") %>' CommandName="View" OnClick="ViewButton_Click" Text="View" />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <p>No claims in the system to process.</p>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>

</asp:Content>