﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class UsersView : System.Web.UI.Page
{
    protected String selectedUserId = null;
    public Boolean newsLetter = false;

    private void BindList(UserAccount user)
    {
        fvUsers.DataSource = UserAccount.convertUserToIList(user);
        fvUsers.DataBind();
    }

    protected Boolean IsAdminUser()
    {
        if (Context.User.Identity.IsAuthenticated)
            return Context.User.IsInRole("admin");

        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string rawId = Request.QueryString["userId"];

        if (!String.IsNullOrEmpty(rawId))
        {
            selectedUserId = rawId;
        }

        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(selectedUserId))
            {
                fvUsers.ChangeMode(FormViewMode.Edit);
                Guid userGuid = new Guid(selectedUserId);
                BindList(UserAccount.getUserInfo(userGuid));
            }
            else
            {
                fvUsers.ChangeMode(FormViewMode.Insert);
            }
        }
    }

    protected UserAccount updateUserData(UserAccount user)
    {
        TextBox tbFirstName = fvUsers.FindControl("tbFirstName") as TextBox;
        TextBox tbLastName = fvUsers.FindControl("tbLastName") as TextBox;
        TextBox tbIdNumber = fvUsers.FindControl("tbIdNumber") as TextBox;
        TextBox tbEmail = fvUsers.FindControl("tbEmail") as TextBox;
        CheckBox cbNewsLetter = fvUsers.FindControl("cbNewsLetter") as CheckBox;
        DropDownList ddlRole = fvUsers.FindControl("ddlRole") as DropDownList;

        user.firstName = tbFirstName.Text;
        user.lastName = tbLastName.Text;

        user.idNumber = tbIdNumber.Text;
        if (!String.IsNullOrEmpty(tbEmail.Text))
            user.email = tbEmail.Text;

        user.role = new String[]{ ddlRole.SelectedValue };

        return user;
    }

    protected void fvUsers_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
        UserAccount user = updateUserData(new UserAccount());

        //get the user password
        TextBox tbPassword = fvUsers.FindControl("tbPassword") as TextBox;
        user.password = tbPassword.Text;

        try
        {
            if (user.registerUser(user.role[0]))
                FlashHelper.FlashInfo("User Account was created successfully.");
            else
                FlashHelper.FlashError("Unable to create User Account");
        }
        catch (Exception ex)
        {
            CustomUtility.standardErrorHandling(ex);
        }
    }

    protected void fvUsers_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        UserAccount user = null;

        if (!String.IsNullOrEmpty(selectedUserId)) 
        {
            user = UserAccount.getUserInfo(new Guid(selectedUserId));
        }

        if (user != null)
        {

            try
            {
                MembershipUser current = Membership.GetUser(user.idNumber);
                //prevent user email changes
                user.email = current.Email;
                //Update the user data
                user = updateUserData(user);

                try
                {
                    user.createUserProfile();
                    BindList(UserAccount.getLoginUserInfo());
                    FlashHelper.FlashInfo("User record updated");
                }
                catch(Exception)
                {
                    FlashHelper.FlashError("Unable to update user record ");
                }
            }
            catch (Exception ex)
            {
                CustomUtility.standardErrorHandling(ex);
            }
        }
    }

}