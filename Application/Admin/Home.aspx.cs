﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Home : System.Web.UI.Page
{
    protected void BindList()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        System.Data.DataSet set = ClaimDB.getAll_Claim_ByStatusOnly((int)UserAccount.userLevel());
        lvClaim.DataSource = set;
        lvClaim.DataBind();

        System.Data.DataSet sSet = TimeTableDB.getAllSemester();
        List<Semester> list = TimeTableDB.readSemester(sSet);
        ddlSemester.DataSource = list;
        ddlSemester.DataTextField = "DisplayField";
        ddlSemester.DataValueField = "Id";
        ddlSemester.DataBind();

        if (list.Count > 0)
        {
            BindPeriod(list[0]);
        }
    }

    protected void BindPeriod(Semester semester)
    {
        ddlPeriod.Items.Clear();
        ddlPeriod.Items.Add(new ListItem("All", semester.DisplayField));
        foreach( Period period in TimeTableDB.generatePeriods(semester))
        {
            ddlPeriod.Items.Add(new ListItem(period.DisplayField, period.DisplayField));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        BindList();
    }

    protected void DataPager1_PreRender(object sender, EventArgs e)
    {
        lvClaim.DataBind();
    }

    protected void btnFilterClaim_Click(object sender, EventArgs e)
    {

        try
        {
            DateTime from, to;

            if (!cbManual.Checked)
            {
                String[] periods = ddlPeriod.SelectedValue.Split('-');
                from = DateTime.ParseExact(periods[0].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                to = DateTime.ParseExact(periods[1].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                from = DateTime.ParseExact(tbFrom.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                to = DateTime.ParseExact(tbTo.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            
            }
            //find semester from period
            List<Semester> semesters = TimeTableDB.readSemester(TimeTableDB.getSemester_ByPeriod(from, to));

            if (semesters.Count > 0)
            {

            }
        }
        catch (Exception)
        {
            FlashHelper.FlashError("Valid date periods are required in the format mm/dd/yyyy.");
        }
    }

    protected string getSemesterDate(string semesterId)
    {
        try
        {
            int Id = Convert.ToInt32(semesterId);
            Semester semester = TimeTableDB._getSemester(Id);
            if (semester != null) return semester.Start_Date.ToString("yyyy-MM-dd") + " to " + semester.End_Date.ToString("yyyy-MM-dd");
        }
        catch (Exception) { };

        return "unknown";
    }

    protected void ViewButton_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        Response.Redirect("/Claim/ClaimView?claimId=" + btn.CommandArgument);
    }

    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(ddlSemester.SelectedValue);
        Semester semester = TimeTableDB._getSemester(id);
        if (semester != null) BindPeriod(semester);
    }

}