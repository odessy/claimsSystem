﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ResponsibilityIndex : System.Web.UI.Page
{

    protected void BindList()
    {
        lvResponsibilities.DataSource = ResponsibilityDB.getAll_Responsibilities();
        lvResponsibilities.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) BindList();
    }

    protected void DataPager1_PreRender(object sender, EventArgs e)
    {
        BindList();
    }

    protected void lvResponsibilities_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;

            e.KeepInEditMode = true;
        }
        else
        {
            FlashHelper.FlashInfo("Responsibility updated");
            lvResponsibilities.DataBind();
        }
    }

    protected void lvResponsibilities_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;
        }
        else
        {
            FlashHelper.FlashInfo("Responsibility deleted");
            lvResponsibilities.DataBind();
        }
    }

    protected void lvResponsibilities_ItemInserted(object sender, ListViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;

            e.KeepInInsertMode = true;
        }
        else
        {
            FlashHelper.FlashInfo("Responsibility inserted");
            lvResponsibilities.DataBind();
        }
    }

    protected void lvResponsibilities_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        lvResponsibilities.EditIndex = -1;
        BindList();
    }

    protected void lvResponsibilities_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        lvResponsibilities.EditIndex = e.NewEditIndex;
        BindList();
    }

    protected void lvResponsibilities_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        TextBox lbCode = (lvResponsibilities.Items[e.ItemIndex].FindControl("tbCode")) as TextBox;
        TextBox lbDescription = (lvResponsibilities.Items[e.ItemIndex].FindControl("tbDescription")) as TextBox;
        TextBox lbHour = (lvResponsibilities.Items[e.ItemIndex].FindControl("tbHour")) as TextBox;
        Label lbResponsibilityId = (lvResponsibilities.Items[e.ItemIndex].FindControl("lbResponsibilityId")) as Label;

        Responsibility responsibility = new Responsibility();
        responsibility.Code = lbCode.Text;
        responsibility.Description = lbDescription.Text;

        try
        {
            responsibility.Hours = Convert.ToInt32(lbHour.Text);
            responsibility.Id = Convert.ToInt32(lbResponsibilityId.Text);

            ResponsibilityDB.updateResponsibility(responsibility);

        }catch(Exception)
        {
            FlashHelper.FlashError("An error occured please try again");  
        }

        lvResponsibilities.EditIndex = -1;
        BindList();
    }

    protected void lvResponsibilities_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        Label lbId = (lvResponsibilities.Items[e.ItemIndex].FindControl("lbResponsibilityId")) as Label;
        Label lbCode = (lvResponsibilities.Items[e.ItemIndex].FindControl("lbCode")) as Label;
        try 
        {
            ResponsibilityDB.deleteResponsibility(Convert.ToInt32(lbId.Text));
            FlashHelper.FlashInfo("Responsibility \"" + lbCode.Text + "\" deleted");
        }
        catch(Exception)
        {
            FlashHelper.FlashError("Unable to delete \"" + lbCode.Text + "\" Responsibility");
        }

        lvResponsibilities.EditIndex = -1;
        BindList();
    }

    protected void lvResponsibilities_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        TextBox tbCode = (e.Item.FindControl("tbCode")) as TextBox;
        TextBox tbDescription = (e.Item.FindControl("tbDescription")) as TextBox;
        TextBox tbHour = (e.Item.FindControl("tbHour")) as TextBox;

        Responsibility responsibility = new Responsibility();
        responsibility.Code = tbCode.Text;
        responsibility.Description = tbDescription.Text;

        try
        {
            responsibility.Hours = Convert.ToInt32(tbHour.Text);

            if (ResponsibilityDB.insertResposnsibility(responsibility) > 0)
            {
                FlashHelper.FlashInfo("Responsibility Inserted");
            }
        }
        catch (Exception) 
        {
            FlashHelper.FlashError("An error occured please try again");      
        }


        lvResponsibilities.EditIndex = -1;
        BindList();
    }
}