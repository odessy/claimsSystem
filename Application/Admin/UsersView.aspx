﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UsersView.aspx.cs" Inherits="UsersView"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Add New User</h1></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:FormView ID="fvUsers" CssClass="form clearfix" runat="server" AllowPaging="false"
        OnItemInserting="fvUsers_ItemInserting"
        OnItemUpdating="fvUsers_ItemUpdating">
        <EditItemTemplate>
            <div class="alert alert-help prepend-top-default"> This information will appear on your profile. </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbFirstName" CssClass="control-label" runat="server" Text="First Name"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbFirstName" runat="server" Text='<%# Bind("firstName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvFirstName" runat="server"
                        ControlToValidate="tbFirstName"
                        ErrorMessage="First Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbLastName" CssClass="control-label" runat="server" Text="Last Name"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbLastName" runat="server" Text='<%# Bind("lastName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvLastName" runat="server"
                        ControlToValidate="tbLastName"
                        ErrorMessage="Last Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbIdNumber" CssClass="control-label" runat="server" Text="ID Number"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbIdNumber" Enabled="false" runat="server" Text='<%# Bind("idNumber") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="tbIdNumber"
                        ErrorMessage="ID Number is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbEmail" CssClass="control-label" runat="server" Text="Email Address"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbEmail" Enabled="false" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvEmail" runat="server"
                        ControlToValidate="tbEmail"
                        ErrorMessage="Email is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <!--<div class="control-div clearfix">
                <asp:Label ID="lbRole" CssClass="control-label" runat="server" Text="User Role(s)"></asp:Label>
                <% //select the role for the current user
                    //must be done before the dropDownList control
                    DropDownList ddl = (fvUsers.FindControl("ddlRole") as DropDownList);
                    object roles = DataBinder.Eval(fvUsers.DataItem, "role");
                    if (ddl != null && roles != null)
                    {
                        if (roles.GetType() != typeof(System.DBNull))
                        {
                            foreach (string role in (string[])roles)
                            {
                                ddl.Items.Add(new ListItem(role, role));
                            }
                        }
                    }
                %>
                <div class="col-sm-10">
                    <asp:DropDownList Enabled="true" ID="ddlRole" runat="server">
                    </asp:DropDownList>
                    <span style="visibility:hidden;">placeholder</span>
                </div>
            </div>-->
            <div class="form-group clearfix">
                <div class="col-sm-offset-2 col-sm-10 ">
                    <asp:Button ID="btnEditUser" CssClass="button large green" 
                        runat="server" CommandName="Update" Text="Update" />
                </div>
            </div>
        </EditItemTemplate>
        <InsertItemTemplate>
            <div class="control-div clearfix">
                <asp:Label ID="lbFirstName" CssClass="control-label" runat="server" Text="Label">First Name</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbFirstName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvFirstName" runat="server"
                        ControlToValidate="tbFirstName"
                        ErrorMessage="First Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbLastName" CssClass="control-label" runat="server" Text="Label">Last Name</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbLastName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvLastName" runat="server"
                        ControlToValidate="tbLastName"
                        ErrorMessage="Last Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbIdNumber" CssClass="control-label" runat="server" Text="Label">Id Number</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbIdNumber" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="tbIdNumber"
                        ErrorMessage="ID Number is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbEmail" CssClass="control-label" runat="server" Text="Label">Email</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvEmail" runat="server"
                        ControlToValidate="tbEmail"
                        ErrorMessage="Email is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbPassword" CssClass="control-label" runat="server" Text="Label">Password</asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbPassword" TextMode="Password" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvPassword" runat="server"
                        ControlToValidate="tbPassword"
                        ErrorMessage="Password is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbRole" CssClass="control-label" runat="server" Text="User Role"></asp:Label>
                <div class="col-sm-10">
                    <asp:DropDownList ID="ddlRole" runat="server">
                        <asp:ListItem Text="Admin" Value="admin"></asp:ListItem>
                        <asp:ListItem Text="Staff" Value="staff"></asp:ListItem>
                        <asp:ListItem Text="Account" Value="account"></asp:ListItem>
                        <asp:ListItem Text="Human Resource" Value="humanResource"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <br />
            <div class="form-group clearfix">
                <div class="col-sm-offset-2 col-sm-10 ">
                    <asp:Button ID="btnInsertUser" CssClass="button large green" runat="server" CommandName="Insert" Text="Insert New User" />
                </div>
            </div>
        </InsertItemTemplate>
        <EmptyDataTemplate>
            No user data found.
        </EmptyDataTemplate>
    </asp:FormView>
</asp:Content>

