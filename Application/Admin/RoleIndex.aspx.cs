﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Collections;

public partial class RoleIndex : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack) BindList();
    }

    private void BindList()
    {
        String [] roles = Roles.GetAllRoles();
        List<UserRole> list = new List<UserRole>();

        foreach (String role in roles)
        {
            UserRole rg = new UserRole(role);
            list.Add(rg);
        }

        lvRole.DataSource = list;
        lvRole.DataBind();     
    }

    protected void DataPager1_PreRender(object sender, EventArgs e)
    {
        lvRole.DataBind();
    }

    protected void lvRole_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;

            e.KeepInEditMode = true;
        }
        else
        {
            FlashHelper.FlashInfo("Role updated");
            lvRole.DataBind();
        }
    }

    protected void lvRole_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;
        }
        else
        {
            FlashHelper.FlashInfo("Role deleted");
            lvRole.DataBind();
        }
    }

    protected void lvRole_ItemInserted(object sender, ListViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            CustomUtility.standardErrorHandling(e.Exception);

            e.ExceptionHandled = true;

            e.KeepInInsertMode = true;
        }
        else
        {
            FlashHelper.FlashInfo("Role inserted");
            lvRole.DataBind();
        }
    }

    protected void lvRole_ItemCanceling(object sender, ListViewCancelEventArgs e)
    {
        lvRole.EditIndex = -1;
        lvRole.DataBind();
    }

    protected void lvRole_ItemEditing(object sender, ListViewEditEventArgs e)
    {
        lvRole.EditIndex = e.NewEditIndex;
        BindList();
    }

    protected void lvRole_ItemUpdating(object sender, ListViewUpdateEventArgs e)
    {
        TextBox lbName = (lvRole.Items[e.ItemIndex].FindControl("tbName")) as TextBox;
        Label lbRoleId = (lvRole.Items[e.ItemIndex].FindControl("lbRoleId")) as Label;

        if (UserRole.deleteRole(lbRoleId.Text))
        {
            UserRole.insertRole(lbName.Text);
        }
        
        lvRole.EditIndex = -1;
        BindList();
    }

    protected void lvRole_ItemDeleting(object sender, ListViewDeleteEventArgs e)
    {
        Label label = (lvRole.Items[e.ItemIndex].FindControl("lbRoleId")) as Label;

        if (UserRole.deleteRole(label.Text))
        {
            FlashHelper.FlashWarning("Role \""+label.Text+"\" deleted");
        }
        else
        {
            FlashHelper.FlashError("Unable to delete \""+label.Text+"\" role");
        }

        lvRole.EditIndex = -1;
        BindList();
    }

    protected void lvRole_ItemInserting(object sender, ListViewInsertEventArgs e)
    {
        TextBox txt = (e.Item.FindControl("tbName")) as TextBox;

        if (UserRole.insertRole(txt.Text))
        {
            FlashHelper.FlashInfo("Role Inserted");
        }

        lvRole.EditIndex = -1;
        BindList();
    }
}