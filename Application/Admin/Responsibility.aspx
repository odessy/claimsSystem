﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Responsibility.aspx.cs" Inherits="Admin_ResponsibilityIndex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Responsibility</h1></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div class="alert alert-help prepend-top-default">Manage Responsibilities</div>
        <div  style="margin-bottom:5px;" >
            <asp:DataPager ID="DataPager1" runat="server" PagedControlID="lvResponsibilities"
                PageSize="10" OnPreRender="DataPager1_PreRender">
                <Fields>
                    <asp:NextPreviousPagerField ButtonType="Button" />
                </Fields>
            </asp:DataPager>
        </div>
        <asp:ListView ID="lvResponsibilities" runat="server" 
            DataKeyNames="Id" 
            OnItemInserted="lvResponsibilities_ItemInserted"
            OnItemDeleted="lvResponsibilities_ItemDeleted"
            OnItemUpdated="lvResponsibilities_ItemUpdated"
            OnItemUpdating="lvResponsibilities_ItemUpdating"
            OnItemInserting="lvResponsibilities_ItemInserting"
            OnItemDeleting="lvResponsibilities_ItemDeleting"
            OnItemEditing="lvResponsibilities_ItemEditing"
            OnItemCanceling ="lvResponsibilities_ItemCanceling"
            InsertItemPosition="LastItem">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th id="Th2" runat="server">ResponsibilityId</th>
                        <th id="Th4" runat="server">Code</th>
                        <th id="Th3" runat="server">Description</th>
                        <th id="Th5" runat="server">Hours</th>
                        <th id="Th1" runat="server">Action(s)</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbResponsibilityId" runat="server" Text='<%# Bind("Id") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbCode" runat="server" Text='<%# Bind("Code") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbDescription" runat="server" Text='<%# Bind("Description") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbHour" runat="server" Text='<%# Bind("Hours") %>' />
                    </td>
                    <td>
                        <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete" />
                        <asp:LinkButton ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </td>
                </tr>
            </ItemTemplate>
            <EditItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbResponsibilityId" runat="server" Text='<%# Bind("Id") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="tbCode" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Code"
                            Text=' <%# Bind("Code") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbCode" runat="server"
                            ControlToValidate="tbCode"
                            ErrorMessage="Code is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbDescription" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Description"
                            Text=' <%# Bind("Description") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbDescription" runat="server"
                            ControlToValidate="tbDescription"
                            ErrorMessage="Description is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbHour" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Hour"
                            Text=' <%# Bind("Hours") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbHour" runat="server"
                            ControlToValidate="tbHour"
                            ErrorMessage="Hour is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                        <asp:LinkButton ID="CancelButton" runat="server" CausesValidation="false" CommandName="Cancel" Text="Cancel" />
                    </td>
                </tr>
            </EditItemTemplate>
            <InsertItemTemplate>
                <tr style="">
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="tbCode" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Code"
                            Text=' <%# Bind("Code") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbCode" runat="server"
                            ControlToValidate="tbCode"
                            ErrorMessage="Code is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbDescription" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Description"
                            Text=' <%# Bind("Description") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbDescription" runat="server"
                            ControlToValidate="tbDescription"
                            ErrorMessage="Description is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:TextBox ID="tbHour" ValidationGroup="insert" CssClass="ignore" runat="server"
                            placeholder="Hour"
                            Text=' <%# Bind("Hours") %>' />
                        <asp:RequiredFieldValidator
                            ValidationGroup="insert"
                            ID="rfvInsertTbHour" runat="server"
                            ControlToValidate="tbHour"
                            ErrorMessage="Hour is a required field.">*
                        </asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:LinkButton ID="InsertButton" runat="server" ValidationGroup="insert" CommandName="Insert" Text="Insert" />
                        <asp:LinkButton ID="CancelButton" runat="server" CausesValidation="false" CommandName="Cancel" Text="Clear" />
                    </td>
                </tr>
            </InsertItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>