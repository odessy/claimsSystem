﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_UserResponsibility : System.Web.UI.Page
{

    private void BindCheckbox(string userId)
    {
        //cbResponsibilities.DataSource = ResponsibilityDB.getAll_Responsibilities();
        //cbResponsibilities.DataTextField = "Description";
        //cbResponsibilities.DataValueField = "Id";
        //cbResponsibilities.DataBind();

        List<Semester> semester = TimeTableDB.readSemester(TimeTableDB.getSemester_Current());

        if (semester.Count > 0)
        {

            List<Responsibility> list = ResponsibilityDB.readResponsibilities(ResponsibilityDB.getAll_Responsibilities());
            foreach (Responsibility res in list)
            {
                ListItem item = new ListItem(res.Description, Convert.ToString(res.Id));
                UserResponsibility respon = ResponsibilityDB._getUserResponsibility(res.Id, new Guid(userId), semester[0].Id);
                if (respon != null) item.Selected = true;
                cbResponsibilities.Items.Add(item);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string userId = Request.QueryString["user"];
            tbUserId.Text = userId;

            BindCheckbox(userId);
        }
        else
        {
            btnSubmit_Click();
        }
    }
    protected void btnSubmit_Click()
    {
        List<Semester> semester = TimeTableDB.readSemester(TimeTableDB.getSemester_Current());

        if (semester.Count > 0)
        {
            UserResponsibility userResponsibility = new UserResponsibility();
            userResponsibility.FK_Semester_Id = semester[0].Id;
            userResponsibility.FK_UserId = new Guid(tbUserId.Text);

            foreach (ListItem checbox in cbResponsibilities.Items)
            {

                userResponsibility.FK_Responsibility = Convert.ToInt32(checbox.Value);
                UserResponsibility respon = ResponsibilityDB._getUserResponsibility(userResponsibility.FK_Responsibility, userResponsibility.FK_UserId, semester[0].Id);
                
                if (checbox.Selected)
                {
                    try
                    {
                        if (respon == null)
                        {
                            ResponsibilityDB.insertUserResposnsibility(userResponsibility);
                            FlashHelper.FlashInfo(checbox.Text + " responsibility added to User: " + userResponsibility.FK_UserId);
                        }
                    }
                    catch (Exception)
                    {
                        FlashHelper.FlashError("An error occured");
                    }
                }
                else
                {
                    //remove responsibility here
                    if (respon != null)
                    {
                        ResponsibilityDB.deleteUserResponsibility(respon.Id);
                        FlashHelper.FlashWarning(checbox.Text + " responsibility removed from User: " + userResponsibility.FK_UserId);
                    }
                }
            }
        }
    }
}