function htmlbodyHeightUpdate() {
    //prevents flashing of black area
    //$('.main').css({ "height": "inherit" });

    var height = $(window).height();
    var height2 = $('.main').height();

    if (height > height2) {
        $('html').height(Math.max(height, height2));
    }
    else {
        //$('.main').height(Math.max(height, height2));
        $('html').height(Math.max(height, height2));
    }
}

$(document).ready(function () {
    htmlbodyHeightUpdate()
    $(window).resize(function () {
        htmlbodyHeightUpdate()
    });

    $(window).scroll(function () {
        height2 = $('.main').height()
        htmlbodyHeightUpdate()
    });

	$('.flash').each(function (index) {
		$('.flash').slideDown('slow');
		$(this).on("click", function () {
			$(this).fadeOut();
		});
	});

	$(".nav li").removeClass("active"); 
	//previously active menu item
	$('a[href="' + this.location.pathname + '"]').parents('li,ul').addClass('active');

});