var current_menu = "";

$(window).bind('hashchange', function() {
 	var changehash = window.location.hash;
	changehash = changehash.substr(1);

 	if(changehash!=current_menu){	
		if($("#"+changehash).length != ""){
			show_menu(changehash, false);
		}
	}
})

function setText(element, text) {
    $(element).text(text);
}

function show_menu(menu,load_from_hash)
{
	if(window.location.hash && load_from_hash)
	{
		var thehash = window.location.hash;
		show_menu(thehash.substr(1),false);
		return false;
	}
	if(current_menu!=menu)
		hide_menu()
	$("#"+menu).show()
	current_menu = menu;	
	$("."+menu).addClass("selected");
	window.location.hash = '#'+menu;
}

function hide_menu()
{
	if(current_menu!='')
	{
		$("#"+current_menu).hide(); 
		$("."+current_menu).removeClass("selected");
		return true;
	}
}

function set_current(current)
{
	current_menu = current;

	if(window.location.hash){
		var currenthash = window.location.hash;
		currenthash = currenthash.substr(1);
		if($("#"+currenthash).length != ""){
			show_menu(currenthash,false);
		}
		
	}
	$("."+current_menu).addClass("selected");
}

