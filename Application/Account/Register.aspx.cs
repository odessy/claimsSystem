﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSignup_Click(object sender, EventArgs e)
    {

        UserAccount user = new UserAccount();
        user.firstName = tbFirstName.Text.Trim();
        user.lastName = tbLastName.Text.Trim();
        user.idNumber = tbIdNumber.Text.Trim();
        user.email = String.IsNullOrEmpty(tbEmail.Text.Trim()) ? null : tbEmail.Text.Trim();
        user.password = tbPassword.Text.Trim();

        if (user.registerUser("staff"))
        {
            FlashHelper.FlashInfo("Your User Account was created successfully.");
            //send the user to the login page
            Response.Redirect("~/Account/Login.aspx");
        }
        else
        {
            FlashHelper.FlashError("Unable to create User Account. Please try again later!");
        }
    }
}