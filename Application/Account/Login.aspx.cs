using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string emailOrIdNumber = tbEmail.Text.Trim();
        string foundUserName = Membership.GetUserNameByEmail(emailOrIdNumber);

        String userName = string.IsNullOrEmpty(foundUserName) ? emailOrIdNumber :  foundUserName;

        if (Membership.ValidateUser(userName, tbPassword.Text.Trim()))
        {
            FormsAuthentication.RedirectFromLoginPage(userName, cbRememberMe.Checked);
        }
        else
        {
            FlashHelper.FlashError("Login failed please check email and password and try again!");
        }
    }
}
