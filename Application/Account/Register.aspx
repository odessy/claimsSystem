﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form small-form center">
        <div class="form-title"><h2>Create an account</h2></div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbFirstName" runat="server" Text="Label">First Name</asp:Label>-->
            <asp:TextBox ID="tbFirstName" runat="server" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvFirstName" runat="server"
                ControlToValidate="tbFirstName"
                ErrorMessage="First Name is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbLastName" runat="server" Text="Label">Last Name</asp:Label>-->
            <asp:TextBox ID="tbLastName" runat="server" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvLastName" runat="server"
                ControlToValidate="tbLastName"
                ErrorMessage="Last Name is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbIdNumber" runat="server" Text="Label">Last Name</asp:Label>-->
            <asp:TextBox ID="tbIdNumber" runat="server" placeholder="ID Number"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server"
                ControlToValidate="tbIdNumber"
                ErrorMessage="ID Number is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbEmail" runat="server" Text="Label">Email</asp:Label>-->
            <asp:TextBox ID="tbEmail" runat="server" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvEmail" runat="server"
                ControlToValidate="tbEmail"
                ErrorMessage="Email is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbPassword" runat="server" Text="Label">Password</asp:Label>-->
            <asp:TextBox ID="tbPassword" TextMode="Password" runat="server" placeholder="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvPassword" runat="server"
                ControlToValidate="tbPassword"
                ErrorMessage="Password is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div">
            <asp:Button ID="btnSignup" CausesValidation="true" CssClass="button large green"
                runat="server" OnClick="btnSignup_Click" Text="Sign Up" />
            <p>
                By clicking on 'Sign Up' above, you confirm that you accept the Terms of Use.
            </p>
            <p>
                Existing user? <a runat="server" href="~/Account/Login.aspx">Sign in.</a>
            </p>
        </div>
    </div>
</asp:Content>