<%@ Page Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="Login"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form small-form center clearfix">
        <div class="form-title"><h2>Sign in</h2></div>
        <div class="control-div clearfix">
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbEmail" runat="server" Text="Label">Email</asp:Label>-->
            <asp:TextBox ID="tbEmail" runat="server" placeholder="Email or UTech ID Number"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvEmail" runat="server"
                ControlToValidate="tbEmail"
                ErrorMessage="Email is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div clearfix">
            <!--<asp:Label ID="lbPassword" runat="server" Text="Label">Password</asp:Label>-->
            <asp:TextBox ID="tbPassword" TextMode="Password" runat="server" placeholder="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ForeColor="Red" ID="rfvPassword" runat="server"
                ControlToValidate="tbPassword"
                ErrorMessage="Password is required.">
            </asp:RequiredFieldValidator>
        </div>
        <div class="control-div">
            <asp:CheckBox ID="cbRememberMe" runat="server" />
            <asp:Label ID="lbRememberMe" runat="server" Text="Label">Remember me</asp:Label>
        </div>
        <div class="control-div clearfix">
            <asp:Button ID="btnLogin" runat="server" CssClass="button large green" Text="Sign In" OnClick="btnLogin_Click" />
            <p>
                <a id="A1" runat="server" href="#">Forgot your password?</a>
            </p>
             <p>
                New user? <a runat="server" href="~/Account/Register.aspx">Create Account.</a>
            </p>
        </div>
    </div>
</asp:Content>
