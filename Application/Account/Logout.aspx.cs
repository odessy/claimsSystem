﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Context.User.Identity.IsAuthenticated)
        {
            Session.Remove("User");
            Session.Remove("Roles");

            FormsAuthentication.SignOut();
        }

        FlashHelper.FlashInfo("Your Logged out!");

        Response.Redirect("~/Home.aspx");
    }
}