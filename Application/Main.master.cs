﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Main : System.Web.UI.MasterPage
{
    //all this to be present in all pages
    protected Boolean IsAdminUser()
    {
        if(Context.User.Identity.IsAuthenticated)
            return Context.User.IsInRole("admin");

        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string xmlPath = Server.MapPath("~/App_Data/XMLMenu.xml");
            XmlDataSource xmlDataSource = new XmlDataSource();
            xmlDataSource.DataFile = xmlPath;

            String role = "Guest";

            if (Context.User.IsInRole("admin"))
                role = "admin";
            else
                if (Context.User.IsInRole("customer")) role = "customer";

            xmlDataSource.XPath = "User/Role[@id='" + role + "']/Menu";

            Menu.DataSource = xmlDataSource;
            Menu.DataBind();
        }
    }

    protected void Page_PreRender()
    {
        //Flash Messages Displaying
        String msg = FlashHelper.Flash(FlashData.get());
        FlashHolder.InnerHtml = msg;
        //clear the message after display
        FlashData.get().clear();
    }

    protected void OnMenuItemDataBound(object sender, MenuEventArgs e)
    {
        if (SiteMap.CurrentNode != null)
        {
            if (e.Item.Text == SiteMap.CurrentNode.Title)
            {
                if (e.Item.Parent != null)
                {
                    e.Item.Parent.Selected = true;
                }
                else
                {
                    e.Item.Selected = true;
                }
            }
        }
    }
}
