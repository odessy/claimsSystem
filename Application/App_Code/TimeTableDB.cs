﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;

/// <summary>
/// Summary description for TimeTableDB
/// </summary>
/// 

public class TimeTable
{
    public int Id { get; set; }
    public object FK_UserId { get; set; }
    public string Name { get; set; }
    public string IdNumber { get; set; }
    public int FK_Semester_Id { get; set; }
};

public class TimeTableEvent
{
    public int Id { get; set; }
    public int FK_TimeTable_Id { get; set; }
    public int Day { get; set; }
    public TimeSpan Start_Time { get; set; }
    public TimeSpan End_Time { get; set; }
    public int FK_Category_Id { get; set; }
    public string FK_Category_Name { get; set; }
    public int FK_Module_Id { get; set; }
    public string Room { get; set; }
};

public class Module
{
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
};

public class Semester
{
    public int Id { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public String DisplayField
    {
        get { return this.Start_Date.ToString("MM/dd/yyyy") + " - " + this.End_Date.ToString("MM/dd/yyyy"); }
    }
};

public class Period
{
    public int Semester_Id { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public String DisplayField
    {
        get { return this.Start_Date.ToString("MM/dd/yyyy") +" - "+ this.End_Date.ToString("MM/dd/yyyy"); }
    }
};

[DataObject(true)]
public class TimeTableDB
{
    //TimeTable Procedures

    public static TimeTable _getTimeTable(Guid userId)
    {
        List<TimeTable> timeTable = readTimeTables(getAllTimeTable_ByUserId(userId));

        if (timeTable.Count > 0)
        {
            return timeTable[0];
        }

        return null;
    }

    public static TimeTable _getTimeTable(string IdNumber)
    {
        List<TimeTable> timeTable = readTimeTables(getAllTimeTable_By_IdNumber(IdNumber));

        if (timeTable.Count > 0)
        {
            return timeTable[0];
        }

        return null;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllTimeTable_ByName(string firstName, string lastName)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getAll_TimeTable_ByName");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addVarCharParameter("@FirstName", firstName);
        con.addVarCharParameter("@LastName", lastName);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllTimeTable_By_IdNumber(string IdNumber)
    {
        return Connection.selectByKeyStoreProcedure("getAll_TimeTable_By_IdNumber", "@IdNumber", IdNumber, SqlDbType.VarChar);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllTimeTable_ByUserId(Guid userId)
    {
        return Connection.selectByKeyStoreProcedure("getAll_TimeTable_ByUserId", "@Id", userId, SqlDbType.UniqueIdentifier);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getTimeTable(int timeTableId)
    {
        return Connection.selectByKeyStoreProcedure("getAll_TimeTable", "@Id", timeTableId, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getTimeTables()
    {
        return Connection.selectAllStoreProcedure("getAll_TimeTable");
    }

    public static List<TimeTable> readTimeTables(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<TimeTable> timeTableList = new List<TimeTable>();

        if (dataTable == null) return timeTableList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            TimeTable timeTable = new TimeTable();
            timeTable.Id = Convert.ToInt32(dataRow["Id"]);
            if (Convert.ToString(dataRow["FK_UserId"]) != "")
                timeTable.FK_UserId = new Guid(Convert.ToString(dataRow["FK_UserId"]));
            timeTable.Name = Convert.ToString(dataRow["Name"]);
            timeTable.IdNumber = Convert.ToString(dataRow["IdNumber"]);
            timeTable.FK_Semester_Id = Convert.ToInt32(dataRow["FK_Semester_Id"]);
            timeTableList.Add(timeTable);
        }

        return timeTableList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertTimeTable(TimeTable timeTable)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertTimeTable");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addVarCharParameter("@Name", timeTable.Name);
        con.addVarCharParameter("@IdNumber", timeTable.IdNumber);
        con.addIntParameter("@FK_Semester_Id", timeTable.FK_Semester_Id);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateTimeTable(TimeTable timeTable)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateTimeTable");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", timeTable.Id);
        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, timeTable.FK_UserId);
        con.addVarCharParameter("@Name", timeTable.Name);
        con.addVarCharParameter("@IdNumber", timeTable.IdNumber);
        con.addIntParameter("@FK_Semester_Id", timeTable.FK_Semester_Id);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteTimeTable(int timeTableId)
    {
        return Connection.deleteByKeyStoreProcedure("deleteTimeTable", "@Id", timeTableId, SqlDbType.Int);
    }

    //TimeTableEvent Procedures

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllTimeTableEvents(int timeTableId)
    {
        return Connection.selectByKeyStoreProcedure("getAll_TimeTableEvents_TimeTable_Id", "@Id", timeTableId, SqlDbType.Int);
    }

    public static List<TimeTableEvent> readTimeTableEvents(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<TimeTableEvent> timeTableEventList = new List<TimeTableEvent>();

        if (dataTable == null) return timeTableEventList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            TimeTableEvent timeTableEvent = new TimeTableEvent();
            timeTableEvent.Id = Convert.ToInt32(dataRow["Id"]);
            timeTableEvent.FK_TimeTable_Id = Convert.ToInt32(dataRow["FK_TimeTable_Id"]);
            timeTableEvent.Day = Convert.ToInt32(dataRow["Day"]);
            timeTableEvent.Start_Time = TimeSpan.Parse(Convert.ToString(dataRow["Start_Time"]));
            timeTableEvent.End_Time = TimeSpan.Parse(Convert.ToString(dataRow["End_Time"]));
            timeTableEvent.FK_Category_Id = Convert.ToInt32(dataRow["FK_Category_Id"]);
            timeTableEvent.FK_Module_Id = Convert.ToInt32(dataRow["FK_Module_Id"]);
            timeTableEvent.Room = Convert.ToString(dataRow["Room"]);
            timeTableEventList.Add(timeTableEvent);
        }

        return timeTableEventList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertTimeTableEvent(TimeTableEvent timeTableEvent)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertTimeTableEvent");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@FK_TimeTable_Id", timeTableEvent.FK_TimeTable_Id);
        con.addIntParameter("@Day", timeTableEvent.Day);
        con.addParameter("@Start_Time", SqlDbType.Time, timeTableEvent.Start_Time);
        con.addParameter("@End_Time", SqlDbType.Time, timeTableEvent.End_Time);
        con.addVarCharParameter("@FK_Category_Name", timeTableEvent.FK_Category_Name);
        con.addIntParameter("@FK_Module_Id", timeTableEvent.FK_Module_Id);
        con.addVarCharParameter("@Room", timeTableEvent.Room);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateTimeTableEvent(TimeTableEvent timeTableEvent)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateTimeTableEvent");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", timeTableEvent.Id);
        con.addIntParameter("@FK_TimeTable_Id", timeTableEvent.FK_TimeTable_Id);
        con.addIntParameter("@Day", timeTableEvent.Day);
        con.addParameter("@Start_Time", SqlDbType.Time, timeTableEvent.Start_Time);
        con.addParameter("@End_Time", SqlDbType.Time, timeTableEvent.End_Time);
        con.addIntParameter("@FK_Category_Id", timeTableEvent.FK_Category_Id);
        con.addIntParameter("@FK_Module_Id", timeTableEvent.FK_Module_Id);
        con.addVarCharParameter("@Room", timeTableEvent.Room);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteTimeTableEvent(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("delete_TimeTable", "@Id", Id, SqlDbType.Int);
    }

    //Module Procedures

    public static Module _getModule(string code)
    {
        List<Module> modules = readModules(getModule_By_Code(code));

        if (modules.Count > 0)
        {
            return modules[0];
        }

        return null;
    }

    public static Module _getModule(int Id)
    {
        List<Module> modules = readModules(getModule_By_Id(Id));

        if (modules.Count > 0)
        {
            return modules[0];
        }

        return null;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getModule_By_Code(string code)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Module_ByCode", "@Value", code, SqlDbType.VarChar);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getModule_By_Id(int Id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Module_ById", "@Id", Id, SqlDbType.Int);
    }

    public static List<Module> readModules(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<Module> moduleList = new List<Module>();

        if (dataTable == null) return moduleList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            Module module = new Module();
            module.Id = Convert.ToInt32(dataRow["Id"]);
            module.Name = Convert.ToString(dataRow["Name"]);
            module.Code = Convert.ToString(dataRow["Code"]);
            moduleList.Add(module);
        }

        return moduleList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertModule(Module module)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertModule");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addVarCharParameter("@Code", module.Code);
        con.addVarCharParameter("@Name", module.Name);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateModule(Module module)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateModule");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", module.Id);
        con.addVarCharParameter("@Code", module.Code);
        con.addVarCharParameter("@Name", module.Name);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    //Semester Procedures

    public static Semester _getSemester(int id)
    {
        List<Semester> semesters = readSemester(getSemester_ById(id));

        if (semesters.Count > 0)
        {
            return semesters[0];
        }

        return null;
    }

    public static Semester _getSemester(string startDate)
    {
        List<Semester> semesters = readSemester(getSemester_ByStart_Date(startDate));

        if (semesters.Count > 0)
        {
            return semesters[0];
        }

        return null;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllSemester()
    {
        return Connection.selectAllStoreProcedure("getAll_Semester");
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getSemester_Current()
    {
        return Connection.selectAllStoreProcedure("get_Semester_Current");
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getSemester_ById(int id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Semester_ById", "@Id", id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getSemester_ByStart_Date(string date)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Semester_ByStart_Date", "@Start_Date", date, SqlDbType.Date);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getSemester_ByPeriod(DateTime from, DateTime to)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getAll_Semester_ByPeriod");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter("@From", SqlDbType.Date, from);
        con.addParameter("@To", SqlDbType.Date, to);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    public static List<Semester> readSemester(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<Semester> semesterList = new List<Semester>();

        if (dataTable == null) return semesterList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            Semester semester = new Semester();
            semester.Id = Convert.ToInt32(dataRow["Id"]);
            semester.Start_Date = Convert.ToDateTime(dataRow["Start_Date"]);
            semester.End_Date = Convert.ToDateTime(dataRow["End_Date"]);
            semesterList.Add(semester);
        }

        return semesterList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertSemester(Semester semester)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertSemester");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter("@Start_Date", SqlDbType.Date, semester.Start_Date);
        con.addParameter("@End_Date", SqlDbType.Date, semester.End_Date);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateSemester(Semester semester)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateSemester");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", semester.Id);
        con.addParameter("@Start_Date", SqlDbType.Date, semester.Start_Date);
        con.addParameter("@End_Date", SqlDbType.Date, semester.End_Date);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    public static List<Period> generatePeriods(Semester semester)
    {
        List<Period> periods = new List<Period>(0);
        DateTime date = semester.Start_Date;
        bool endPre = false;

        while (date < semester.End_Date)
        {
            Period period = new Period();
            period.Semester_Id = semester.Id;

            if (date > DateTime.Now)
            {
                endPre = true;
                break;
            }

            period.Start_Date = date;
            date = date.AddMonths(1);
            period.End_Date = date;
            periods.Add(period);
        }

        if( !endPre )
            periods[periods.Count - 1].End_Date = semester.End_Date;

        return periods;
    }
}