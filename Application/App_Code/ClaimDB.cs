﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;

public class ClaimTimeTableEvent
{
    public int FK_TimeTableEvent { get; set; }
    public int FK_Claim { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public int Id { get; set; }
    public int FK_User_Responsibility { get; set; }
}

public class ClaimReview
{
    public int Id { get; set; }
    public int FK_Claim_Id { get; set; }
    public int FK_Message_Id { get; set; }
}

public class Claim
{
    public int Id { get; set; }
    public int Total_Minutes { get; set; }
    public decimal Rate_Per_hour { get; set; }
    public decimal Amount { get; set; }
    public decimal Total_Taxable_Claim { get; set; }
    public decimal Total_Allowances { get; set; }
    public decimal Total_Claim { get; set; }
    public object FK_UserId { get; set; }
    /**
     *  Claim by Status
     *  0 - Open
     *  1 - Submited
     *  2 - Processing
     *  3 - Completed
     * */
    public int Status { get; set; }
    public int FK_Semester_Id { get; set; }
    public DateTime Date_Created { get; set; }
    public DateTime Date_Modified { get; set; }
    public DateTime Period_From { get; set; }
    public DateTime Period_To { get; set; }
}

public class ClaimSummary
{
    public ClaimSummary() { }
    public ClaimSummary(ClaimSummary summary)
    {
        Id = summary.Id;
        FK_TimeTableEvent = summary.FK_TimeTableEvent;
        FK_Claim = summary.FK_Claim;
        Start_Date = summary.Start_Date;
        End_Date = summary.End_Date;
        Description = summary.Description;
        Code = summary.Code;
        FK_UserId = summary.FK_UserId;
        FK_Semester_Id = summary.FK_Semester_Id;
        ModuleCode = summary.ModuleCode;
        Day = summary.Day;
        hours = summary.hours;
        Name = summary.Name;
    }

    public int Id { get; set; }
    public int FK_TimeTableEvent { get; set; }
    public int FK_Claim { get; set; }
    public DateTime Start_Date { get; set; }
    public DateTime End_Date { get; set; }
    public string Description { get; set; }
    public string Code { get; set; }
    public Guid FK_UserId { get; set; }
    public int FK_Semester_Id { get; set; }
    public string ModuleCode { get; set; }
    public string Day { get; set; }
    public string hours { get; set; }
    public string Name { get; set; }
}

public class ClaimSummary2
{
    public ClaimSummary2() { }
    public ClaimSummary2(ClaimSummary2 claimsummary)
    {
        Id = claimsummary.Id;
        FK_Claim = claimsummary.FK_Claim;
        DatesWorked = claimsummary.DatesWorked;
        Code = claimsummary.Code;
        FK_UserId = claimsummary.FK_UserId;
        FK_Semester = claimsummary.FK_Semester;
        TimeWorked = claimsummary.TimeWorked;
        ModuleCode = claimsummary.ModuleCode;
        TotalFH = claimsummary.TotalFH;
        TotalOH = claimsummary.TotalOH;
        Day = claimsummary.Day;
        Hours = claimsummary.Hours;
    }

    public int Id {get; set;}
    public int FK_Claim { get; set; }
    public string DatesWorked { get; set; }
    public string Code { get; set; }
    public Guid FK_UserId { get; set; }
    public int FK_Semester { get; set; }
    public string TimeWorked { get; set; }
    public string ModuleCode { get; set; }
    public double TotalFH { get; set; }
    public double TotalOH { get; set; }
    public string Day { get; set; }
    public string Hours { get; set; }
}

public class ClaimDB
{
    //Claim

    public static string getStringStatus(int status)
    {
        switch (status)
        {
            case 0: return "Open: Claim not submited";
            case 1: return "Processing";
            case 2: return "Stage 2: Sent to accounts";
            case 3: return "Completed";
            case 4: return "Error: Invalid Claim";
            default: return "Unknown";
        }
    }

    public static string getStringStatusClass(int status)
    {
        switch (status)
        {
            case 0: return "black";
            case 3: return "green";
            case 4: return "red";
            default: return "blue";
        }
    }

    public static List<ClaimSummary2> getClaimSummary(Claim claim)
    {
        Dictionary<int, List<ClaimSummary>> slist = ClaimDB.readClaimSummary(ClaimDB.getAll_ClaimTimeTableEvents_Claim_Id_Group(claim.Id));
        List<ClaimSummary2> cs2list = new List<ClaimSummary2>();
        foreach (int x in slist.Keys)
        {
            ClaimSummary2 cs2 = new ClaimSummary2();
            if (slist[x].Count > 0)
            {
                cs2.Code = slist[x][0].Code;
                cs2.Day = slist[x][0].Day;
                cs2.FK_Claim = slist[x][0].FK_Claim;
                cs2.FK_UserId = slist[x][0].FK_UserId;
                cs2.ModuleCode = slist[x][0].ModuleCode;

                TimeSpan oStart_Time = TimeSpan.Parse(slist[x][0].Start_Date.ToString("HH:mm:ss"));
                TimeSpan oEnd_Time = TimeSpan.Parse(slist[x][0].End_Date.ToString("HH:mm:ss"));

                cs2.TimeWorked = CustomUtility.formateTime(oStart_Time.ToString()) + " - " + CustomUtility.formateTime(oEnd_Time.ToString());
                cs2.TotalOH = slist[x].Count * ((oEnd_Time - oStart_Time).TotalHours);

                Dictionary<string, List<ClaimSummary2>> diclist = new Dictionary<string, List<ClaimSummary2>>();
                foreach (ClaimSummary cs in slist[x])
                {
                    string key = cs.Start_Date.ToString("HH:mm:ss") + cs.End_Date.ToString("HH:mm:ss");
                    if (!diclist.ContainsKey(key))
                        diclist[key] = new List<ClaimSummary2>();

                    TimeSpan Start_Time = TimeSpan.Parse(cs.Start_Date.ToString("HH:mm:ss"));
                    TimeSpan End_Time = TimeSpan.Parse(cs.End_Date.ToString("HH:mm:ss"));

                    ClaimSummary2 cs3 = new ClaimSummary2(cs2);
                    cs3.TimeWorked = CustomUtility.formateTime(Start_Time.ToString()) + " - " + CustomUtility.formateTime(End_Time.ToString());
                    cs3.TotalOH = ((End_Time - Start_Time).TotalHours);
                    cs3.DatesWorked = cs.Start_Date.ToString("dd/MM");

                    diclist[key].Add(cs3);
                }

                foreach (string key in diclist.Keys)
                {
                    //cs2list.Add(cs2);
                    ClaimSummary2 cs4 = new ClaimSummary2(cs2);
                    cs4.TotalOH = 0;
                    cs4.DatesWorked = "";

                    int count = 0;
                    foreach (ClaimSummary2 cs5 in diclist[key])
                    {
                        cs4.TimeWorked = cs5.TimeWorked;
                        cs4.TotalOH += cs5.TotalOH;
                        cs4.DatesWorked += cs5.DatesWorked;
                        if (count < diclist[key].Count - 1)
                            cs4.DatesWorked += ", ";
                        count++;
                    }

                    claim.Rate_Per_hour = 1000;
                    claim.Total_Minutes += (int)(cs4.TotalOH * 3600);
                    claim.Total_Claim += (decimal)(cs4.TotalOH * (double)claim.Rate_Per_hour);
                    claim.Total_Taxable_Claim = claim.Total_Claim;

                    cs2list.Add(cs4);
                }
            }
        }

        return cs2list;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getClaims()
    {
        return Connection.selectAllStoreProcedure("getAll_Claim");
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getClaims_ByUserId(Guid userId)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Claim_ByUserId", "@FK_UserId", userId, SqlDbType.UniqueIdentifier);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getClaim_ById(int Id)
    {
        return Connection.selectByKeyStoreProcedure("getClaim_ById", "@Id", Id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_Claim_ByStatusOnly(int status)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getAll_Claim_ByStatusOnly");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Status", status);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_Claim_ByStatus(int status, Guid userId)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getAll_Claim_ByStatus");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Status", status);
        con.addParameter("@UserId", SqlDbType.UniqueIdentifier, userId);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    public static List<Claim> readClaims(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<Claim> claimList = new List<Claim>();

        if (dataTable == null) return claimList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            Claim claim = new Claim();
            claim.Id = Convert.ToInt32(dataRow["Id"]);
            if (Convert.ToString(dataRow["FK_UserId"]) != "")
                claim.FK_UserId = new Guid(Convert.ToString(dataRow["FK_UserId"]));
            claim.Total_Minutes = Convert.ToInt32(dataRow["Total_Minutes"]);
            claim.Rate_Per_hour = Convert.ToDecimal(dataRow["Rate_Per_hour"]);
            claim.Amount = Convert.ToDecimal(dataRow["Amount"]);
            claim.Total_Taxable_Claim = Convert.ToDecimal(dataRow["Total_Taxable_Claim"]);
            claim.Total_Claim = Convert.ToDecimal(dataRow["Total_Claim"]);
            claim.FK_UserId = new Guid(Convert.ToString(dataRow["FK_UserId"]));
            claim.Status = Convert.ToInt32(dataRow["Status"]);
            claim.FK_Semester_Id = Convert.ToInt32(dataRow["FK_Semester_Id"]);
            claim.Date_Created = Convert.ToDateTime(dataRow["Date_Created"]);
            claim.Date_Modified = Convert.ToDateTime(dataRow["Date_Modified"]);
            claim.Period_From = Convert.ToDateTime(dataRow["Period_From"]);
            claim.Period_To = Convert.ToDateTime(dataRow["Period_To"]);
            claimList.Add(claim);
        }

        return claimList;
    }

    public static Dictionary<int, List<ClaimSummary>> readClaimSummary(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        Dictionary<int, List<ClaimSummary>> claimSummaryList = new Dictionary<int, List<ClaimSummary>>();

        if (dataTable == null) return claimSummaryList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            ClaimSummary claimSummary = new ClaimSummary();
            claimSummary.Id = Convert.ToInt32(dataRow["Id"]);
            claimSummary.FK_TimeTableEvent = Convert.ToInt32(dataRow["FK_TimeTableEvent"]);
            claimSummary.FK_Claim = Convert.ToInt32(dataRow["FK_Claim"]);
            claimSummary.Start_Date = Convert.ToDateTime(dataRow["Start_Date"]);
            claimSummary.End_Date = Convert.ToDateTime(dataRow["End_Date"]);
            claimSummary.Description = Convert.ToString(dataRow["Description"]);
            claimSummary.Code = Convert.ToString(dataRow["Code"]);
            if (Convert.ToString(dataRow["FK_UserId"]) != "")
                claimSummary.FK_UserId = new Guid(Convert.ToString(dataRow["FK_UserId"]));
            claimSummary.FK_Semester_Id = Convert.ToInt32(dataRow["FK_Semester_Id"]);
            claimSummary.ModuleCode = Convert.ToString(dataRow["ModuleCode"]);
            claimSummary.Day = ScrapeTimeTable.TimeTableEvent.dayToString((ScrapeTimeTable.TimeTableEvent.DAYS)Convert.ToInt32(dataRow["Day"]));
            claimSummary.Name = Convert.ToString(dataRow["Name"]);
            if (!claimSummaryList.ContainsKey(claimSummary.FK_TimeTableEvent))
            {
                claimSummaryList[claimSummary.FK_TimeTableEvent] = new List<ClaimSummary>();
            }
            claimSummaryList[claimSummary.FK_TimeTableEvent].Add(claimSummary);
        }

        return claimSummaryList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertClaim(Claim claim)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertClaim");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Total_Minutes", claim.Total_Minutes);
        con.addIntParameter("@Rate_Per_Hour", claim.Rate_Per_hour);
        con.addParameter("@Amount", SqlDbType.Money, claim.Amount);
        con.addParameter("@Total_Taxable_Claim", SqlDbType.Decimal, claim.Total_Taxable_Claim);
        con.addParameter("@Total_Allowances", SqlDbType.Money, claim.Total_Allowances);
        con.addParameter("@Total_Claim", SqlDbType.Money, claim.Total_Claim);
        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, claim.FK_UserId);
        con.addIntParameter("@Status", claim.Amount);
        con.addIntParameter("@FK_Semester_Id", claim.FK_Semester_Id);
        con.addParameter("@Period_From", SqlDbType.Date, claim.Period_From);
        con.addParameter("@Period_To", SqlDbType.Date, claim.Period_To);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateClaim(Claim claim)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateClaim");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", claim.Id);
        con.addIntParameter("@Total_Minutes", claim.Total_Minutes);
        con.addIntParameter("@Rate_Per_Hour", claim.Rate_Per_hour);
        con.addParameter("@Amount", SqlDbType.Money, claim.Amount);
        con.addParameter("@Total_Taxable_Claim", SqlDbType.Decimal, claim.Total_Taxable_Claim);
        con.addParameter("@Total_Allowances", SqlDbType.Money, claim.Total_Allowances);
        con.addParameter("@Total_Claim", SqlDbType.Money, claim.Total_Claim);
        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, claim.FK_UserId);
        con.addIntParameter("@Status", claim.Status);
        con.addIntParameter("@FK_Semester_Id", claim.FK_Semester_Id);
        con.addParameter("@Date_Modified", SqlDbType.DateTime, DateTime.Now);
        con.addParameter("@Period_From", SqlDbType.Date, claim.Period_From);
        con.addParameter("@Period_To", SqlDbType.Date, claim.Period_To);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteClaim(int claimId)
    {
        return Connection.deleteByKeyStoreProcedure("deleteClaim", "@Id", claimId, SqlDbType.Int);
    }

    //ClaimTimeTableEvent

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_ClaimTimeTableEvents_Claim_Id(int id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_ClaimTimeTableEvents_Claim_Id", "@Id", id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_ClaimTimeTableEvents_Claim_Id_Group(int id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_ClaimTimeTableEvents_Claim_Id_Group", "@Id", id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_ClaimTimeTableEvents_TimeTableEvent_Id(int id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_ClaimTimeTableEvents_TimeTableEvent_Id", "@Id", id, SqlDbType.Int);
    }

    public static List<ClaimTimeTableEvent> readClaimTimeTableEvents(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<ClaimTimeTableEvent> claimTimeTableEventList = new List<ClaimTimeTableEvent>();

        if (dataTable == null) return claimTimeTableEventList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            ClaimTimeTableEvent claimTimeTableEvent = new ClaimTimeTableEvent();
            claimTimeTableEvent.Id = Convert.ToInt32(dataRow["Id"]);
            claimTimeTableEvent.FK_TimeTableEvent = Convert.ToInt32(dataRow["FK_TimeTableEvent"]);
            claimTimeTableEvent.FK_Claim = Convert.ToInt32(dataRow["FK_Claim"]);
            claimTimeTableEvent.Start_Date = Convert.ToDateTime(dataRow["Start_Date"]);
            claimTimeTableEvent.End_Date = Convert.ToDateTime(dataRow["End_Date"]);
            claimTimeTableEvent.FK_User_Responsibility = Convert.ToInt32(dataRow["FK_User_Responsibility"]);
            claimTimeTableEventList.Add(claimTimeTableEvent);
        }

        return claimTimeTableEventList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertClaimTimeTableEvent(ClaimTimeTableEvent claimTimeTableEvent)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertClaim_TimeTableEvent");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@FK_TimeTableEvent", claimTimeTableEvent.FK_TimeTableEvent);
        con.addIntParameter("@FK_User_Responsibility", claimTimeTableEvent.FK_User_Responsibility);
        con.addIntParameter("@FK_Claim", claimTimeTableEvent.FK_Claim);
        con.addParameter("@Start_Date", SqlDbType.DateTime, claimTimeTableEvent.Start_Date);
        con.addParameter("@End_Date", SqlDbType.DateTime, claimTimeTableEvent.End_Date);

        int insert = 0;

        try
        {
            con.openConnection();
            object a = con.executeScalar();
            if(a != null) insert = (int)a;
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateClaimTimeTableEvent(ClaimTimeTableEvent claimTimeTableEvent)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("updateClaim_TimeTableEvent");

        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", claimTimeTableEvent.Id);
        con.addIntParameter("@FK_TimeTableEvent", claimTimeTableEvent.FK_TimeTableEvent);
        con.addIntParameter("@FK_User_Responsibility", claimTimeTableEvent.FK_User_Responsibility);
        con.addIntParameter("@FK_Claim", claimTimeTableEvent.FK_Claim);
        con.addParameter("@Start_Date", SqlDbType.DateTime, claimTimeTableEvent.Start_Date);
        con.addParameter("@End_Date", SqlDbType.DateTime, claimTimeTableEvent.End_Date);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteClaimTimeTableEvent(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("deleteClaimTimeTableEvent", "@Id", Id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteAll_ClaimTimeTableEvent(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("deleteAll_ClaimTimeTableEvent", "@Id", Id, SqlDbType.Int);
    }

    //Claim Review
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertClaimReview(ClaimReview claimReview)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("insertClaimReview");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@FK_Claim_Id", claimReview.FK_Claim_Id);
        con.addIntParameter("@FK_Message_Id", claimReview.FK_Message_Id);

        int insert = 0;

        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }

        return insert;
    }
}