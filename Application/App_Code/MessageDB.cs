﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Data;

/// <summary>
/// Summary description for MessageDB
/// </summary>

public class MessageObj{
    public int Id { get; set; }
    public object From_UserId { get; set; }
    public object To_UserId { get; set; }
    public String Message { get; set; }
    public DateTime Received_Date { get; set; }
    public String Subject { get; set; }
    public int Status { get; set; }
};

public class MessageDB
{
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllMessage_ById(int Id)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Message_ById", "@Id", Id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllMessage_ByUserId(Guid FK_UserId)
    {
        return Connection.selectByKeyStoreProcedure("getAll_Message_ByUserId", "@FK_UserId", FK_UserId, SqlDbType.UniqueIdentifier);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getClaimReview_ById(int Id)
    {
        return Connection.selectByKeyStoreProcedure("getClaim_Review_ById", "@FK_Claim_Id", Id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_Message()
    {
        return Connection.selectAllStoreProcedure("getAllMessage");
    }

    public static int insertMessage(MessageObj Message)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("insertMessage");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter("@FK_From_UserId", SqlDbType.UniqueIdentifier, Message.From_UserId);
        con.addParameter("@FK_To_UserId", SqlDbType.UniqueIdentifier, Message.To_UserId);
        con.addVarCharParameter("@Message", Message.Message);
        con.addParameter("@Recieved_Date", SqlDbType.DateTime, Message.Received_Date);
        con.addVarCharParameter("@Subject", Message.Subject);
        con.addIntParameter("@Status", Message.Status);

        int insert = 0;
        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }
        return insert;
    }

    public static List<MessageObj> readMessage(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<MessageObj> userResponsibilityList = new List<MessageObj>();

        if (dataTable == null) return userResponsibilityList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            MessageObj message = new MessageObj();
            message.Id = Convert.ToInt32(dataRow["Id"]);
            message.Message = Convert.ToString(dataRow["Message"]);
            message.Received_Date = Convert.ToDateTime(dataRow["Recieved_Date"]);
            message.Subject = Convert.ToString(dataRow["Subject"]);
            message.From_UserId = new Guid(Convert.ToString(dataRow["FK_From_UserId"]));
            message.To_UserId = new Guid(Convert.ToString(dataRow["FK_To_UserId"]));
            message.Status = Convert.ToInt32(dataRow["Status"]);
            userResponsibilityList.Add(message);
        }

        return userResponsibilityList;
    }

    public static int updateMessageStatus(MessageObj Message)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("updateMessage_Status");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", Message.Id);
        con.addIntParameter("@Status", Message.Status);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }
    
    public static int updateMessage(MessageObj Message)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("updateMessage");
        con.setSqlCommandType(CommandType.StoredProcedure);
        
        con.addIntParameter("@Id", Message.Id);
        con.addParameter("@FK_From_UserId", SqlDbType.UniqueIdentifier, Message.From_UserId);
        con.addParameter("@FK_To_UserId", SqlDbType.UniqueIdentifier, Message.To_UserId);
        con.addVarCharParameter("@Message", Message.Message);
        con.addParameter("@Recieved_Date", SqlDbType.DateTime, Message.Received_Date);
        con.addVarCharParameter("@Subject", Message.Subject);
        con.addIntParameter("@Status", Message.Status);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    public static int deleteMessage(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("deleteMessage", "@Id", Id, SqlDbType.Int);
    }
}