﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScrapeTimeTable
{
    /// <summary>
    /// Summary description for ReadXMLfromFile.
    /// </summary>
    class ReadXMLfromFile
    {
        public XmlDocument xmlDoc { get; set; }
        public string file { get; set; }

        public ReadXMLfromFile(string file)
        {

            xmlDoc = new XmlDocument();
            xmlDoc.Load(file);
        }
       
        public XmlNodeList getElements(string name)
        {
            return xmlDoc.GetElementsByTagName(name);
        }

        public XmlNodeList getElementsFromXml(string xml, string name)
        {
            XmlDocument tmp = new XmlDocument();
            tmp.LoadXml(xml);
            return tmp.GetElementsByTagName(name);
        }

        public void xmlTextReader()
        {
            XmlTextReader reader = new XmlTextReader(file);

            while (reader.Read())
            {
                switch (reader.NodeType) 
                {
                    case XmlNodeType.Element: // The node is an element.
                        Console.Write("<" + reader.Name);
                        Console.WriteLine(">");
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine (reader.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        Console.Write("</" + reader.Name);
                        Console.WriteLine(">");
                        break;
                }
            }

            Console.ReadLine();
        }
    };
}

