﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrapeTimeTable
{
    public class TimeTableEvent
    {
        public int day { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string category { get; set; }
        public string moduleCode { get; set; }
        public string moduleName { get; set; }
        public string room { get; set; }

        public enum DAYS
        {
            MONDAY = 0, TUESDAY, WEDNESDAY, THURDAY,
            FRIDAY, SATURDAY, SUNDAY
        };

        public static string dayToString(DAYS day)
        {
            switch (day)
            {
                case DAYS.MONDAY:
                {
                    return "Monday";
                }
                case DAYS.TUESDAY:
                {
                    return "Tuesday";
                }
                case DAYS.WEDNESDAY:
                {
                    return "Wednesday";
                }
                case DAYS.THURDAY:
                {
                    return "Thurday";
                }
                case DAYS.FRIDAY:
                {
                    return "Friday";
                }
                case DAYS.SATURDAY:
                {
                    return "Saturday";
                }
                case DAYS.SUNDAY:
                {
                    return "Sunday";
                }
            }

            return null;
        }

        public void display()
        {
            Console.WriteLine("Day: " + dayToString((DAYS)day));
            Console.WriteLine("StartTime: " + startTime);
            Console.WriteLine("EndTime: " + endTime);
            Console.WriteLine("Category: " + category);
            Console.WriteLine("Module Code: " + moduleCode);
            Console.WriteLine("Module Name: " + moduleName);
            Console.WriteLine("Room: " + room);
        }
    }
}
