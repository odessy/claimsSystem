﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangFireJobs
{
	public class Scrape
	{
        public static void ScrapTimeTable(string path)
        {
            
            List<ScrapeTimeTable.Staff> staffs = 
                ScrapeTimeTable.Program.operation(path, "http://10.10.10.104/timetable/", "finder2.html", "Staff");

            foreach (ScrapeTimeTable.Staff staff in staffs)
            {

                //Find timeTable if exist if database first
                TimeTable dbTable = new TimeTable();
                if (staff.timeTables.Count == 1)
                {
                    ScrapeTimeTable.TimeTable table = staff.timeTables[0];

                    //Get Semester Id if exist
                    Semester semester = TimeTableDB._getSemester(table.start.ToString());
                    if (semester == null)
                    {
                        semester = new Semester();
                        semester.Start_Date = table.start;
                        semester.End_Date = table.end;

                        semester.Id = TimeTableDB.insertSemester(semester);
                        if (semester.Id == 0) break;
                    }

                    dbTable.Name = staff.name;
                    dbTable.IdNumber = staff.id;
                    dbTable.FK_Semester_Id = semester.Id;
                    //insert and get timetable id
                    dbTable.Id = TimeTableDB.insertTimeTable(dbTable);

                    if (dbTable.Id > 0)
                    {

                        foreach (ScrapeTimeTable.TimeTableEvent @event in table.timeTableEvents)
                        {
                            //TODO add module to hash map to reduce calls to the database
                            //Get Module Id if exist
                            Module module = new Module();
                            module.Id = 0;

                            if (@event.moduleCode == null)
                            {
                                @event.moduleCode = "EMPTY"; // set to no module in database
                            }

                            module = TimeTableDB._getModule(@event.moduleCode);
                            if (module == null)
                            {
                                //Insert the module
                                module = new Module();
                                module.Name = @event.moduleName;
                                module.Code = @event.moduleCode;
                                module.Id = TimeTableDB.insertModule(module);
                            }

                            //set event category to empty if string 
                            if (@event.category == "")
                            {
                                @event.category = "EMPTY";
                            }

                            TimeTableEvent timeTableEvent = new TimeTableEvent();
                            if (@event.room == null) @event.room = "NULL";
                            timeTableEvent.Room = @event.room;
                            timeTableEvent.Start_Time = getTimeSpan(@event.startTime);
                            timeTableEvent.End_Time = getTimeSpan(@event.endTime);
                            timeTableEvent.Day = @event.day;
                            timeTableEvent.FK_TimeTable_Id = dbTable.Id;
                            timeTableEvent.FK_Module_Id = module.Id;
                            timeTableEvent.FK_Category_Name = @event.category;
                            //insert the timetable event
                            TimeTableDB.insertTimeTableEvent(timeTableEvent);
                        }
                    }
                }
            }

        }

        public static TimeSpan getTimeSpan(string time)
        {
            time = time.Trim().ToLower();

            //char m = time.ElementAt(time.Length - 1);
            //char a = time.ElementAt(time.Length - 2);

            //int find = time.IndexOf(':');

            //int hour = Convert.ToInt32(time.Substring(0, find));
            //int minutes = Convert.ToInt32(time.Substring(find+1, 2)); 

            //if ( (a == 'a' || ( a == 'p' && ( hour < 12 && (hour += 12) > 0 ) ) ) && m == 'm')
            //{
            //    return new TimeSpan(hour, minutes, 0);
            //}

            //return new TimeSpan(0, 0, 0);

            try
            {
                DateTime date = Convert.ToDateTime(time);
                return TimeSpan.Parse(date.ToString("HH:mm:00"));
            }
            catch
            {
                return new TimeSpan(0, 0, 0);
            }
        }
	}
}