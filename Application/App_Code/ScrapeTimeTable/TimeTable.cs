﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrapeTimeTable
{
    public class TimeTable
    {
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public List<TimeTableEvent> timeTableEvents { get; set; }

        public TimeTable()
        {
            timeTableEvents = new List<TimeTableEvent>(0);
        }

        public void addEvent(TimeTableEvent @event)
        {
            timeTableEvents.Add(@event);
        }

        public void addDescription(string description)
        {
            string[] parts = description.Split(',');
            if (parts.Length == 2)
            {
                string[] dates = parts[1].Split('-');
                if (dates.Length == 2)
                {
                    string startDate = dates[0];
                    string endDate = dates[1];

                    start = DateTime.Parse(startDate);
                    end = DateTime.Parse(endDate);
                }
            }
        }

        public void display()
        {
            Console.WriteLine("StartDate: " + start.ToShortDateString());
            Console.WriteLine("EndDate: " + end.ToShortDateString());
            foreach (TimeTableEvent @event in timeTableEvents)
            {
                Console.WriteLine();
                @event.display();
            }
        }
    }
}
