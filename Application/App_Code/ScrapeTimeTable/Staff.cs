﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScrapeTimeTable
{
    public class Staff
    {
        public string name { get; set; }
        public string id { get; set; }
        public List<TimeTable> timeTables { get; set; }

        public Staff()
        {
            name = "";
            id = "";
            timeTables = new List<TimeTable>(0);
        }

        public void addTimeTable(TimeTable timeTable)
        {
            timeTables.Add(timeTable);
        }

        public void display()
        {
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Id: " + id);
            Console.WriteLine("\nTimeTable(s)\n");
            foreach(TimeTable timeTable in timeTables)
                timeTable.display();
        }
    }
}
