﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace ScrapeTimeTable
{
    class Program
    {
        public static void DownloadFile(string remoteFilename, string localFilename)
        {
            try
            {
                WebClient client = new WebClient();
                client.DownloadFile(remoteFilename, localFilename);
            }
            catch (WebException ex)
            {
                //Console.WriteLine(ex.Message);
                //Console.ReadLine();
                Environment.Exit(-1);
            }
        }

        public static Staff processFile(string fileName)
        {
            ReadXMLfromFile x = new ReadXMLfromFile(fileName);
            XmlNodeList events = x.getElements("event");
            XmlNodeList resource = null;

            Staff staff = new Staff();
            TimeTable timeTable = new TimeTable();

            try
            {
                foreach (XmlNode node in events)
                {
                    TimeTableEvent @event = new TimeTableEvent();

                    XmlNodeList day = x.getElementsFromXml(node.OuterXml, "day");
                    XmlNodeList startTime = x.getElementsFromXml(node.OuterXml, "starttime");
                    XmlNodeList endTime = x.getElementsFromXml(node.OuterXml, "endtime");
                    XmlNodeList category = x.getElementsFromXml(node.OuterXml, "category");

                    resource = x.getElementsFromXml(node.OuterXml, "resources");
                    XmlNodeList module = x.getElementsFromXml(resource[0].OuterXml, "module");
                    XmlNodeList room = x.getElementsFromXml(resource[0].OuterXml, "room");

                    if (day.Count > 0)
                        @event.day = Convert.ToInt32(day[0].InnerText);
                    if (startTime.Count > 0)
                        @event.startTime = startTime[0].InnerText;
                    if (endTime.Count > 0)
                        @event.endTime = endTime[0].InnerText;
                    if (category.Count > 0)
                    {
                        @event.category = category[0].InnerText.Trim().ToLower();
                    }
                    if (module.Count > 0)
                    {
                        string moduleTxt = module[0].FirstChild.FirstChild.InnerText.Trim();
                        int index = moduleTxt.IndexOf(' ');
                        if (index > 0)
                        {
                            @event.moduleCode = moduleTxt.Substring(0, index);
                            int index2 = moduleTxt.IndexOf('(');
                            if (index > 0)
                            {
                                index = index2 + 1;
                            }
                            int length = (moduleTxt.Length - 1) - index;
                            @event.moduleName = moduleTxt.Substring(index, length);
                        }
                    }
                    if(room.Count > 0)
                        @event.room = room[0].FirstChild.FirstChild.InnerText;

                    timeTable.addEvent(@event);
                }

                if (resource != null)
                {
                    XmlNodeList staffNode = x.getElementsFromXml(resource[0].OuterXml, "staff");
                    staff.name = staffNode[0].InnerText;
                }

                XmlNodeList span = x.getElements("span");
                XmlNodeList description = x.getElementsFromXml(span[0].OuterXml, "description");

                timeTable.addDescription(description[0].InnerText);
                staff.addTimeTable(timeTable);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }

            return staff;
        }

        public static List<Staff> operation(string path, string baseUrl, string finder, string findString)
        {
            DownloadFile(baseUrl + finder, path + finder);
            List<Staff> staffs = new List<Staff>(0);
            List<LinkItem> links = LinkFinder.Find(path + finder);

            foreach (LinkItem link in links)
            {
                string[] parts = link.Text.Split(':');
                if (parts.Length == 2)
                {
                    if (parts[0].Equals(findString))
                    {
                        //Console.Write(parts[1] +" ");
                        //Console.Write(link.Href);
                        //Console.WriteLine();
                        string fileName = link.Href.Split('.')[0] + ".xml";
                        DownloadFile(baseUrl + fileName, path + fileName);
                        //process staff
                        Staff staff = processFile(path + fileName);
                        if (staff != null) staffs.Add(staff);
                        //delete file
                        File.Delete(path + fileName);
                    }
                }
            }

            return staffs;
        }

        public static void ExecuteScrapeTimetable()
        {
            operation("", "http://10.10.10.104/timetable/", "finder2.html", "Staff");
        }
    }
}
