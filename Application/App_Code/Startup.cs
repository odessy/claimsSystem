﻿using Hangfire;
using Microsoft.Owin;
using Owin;
using System.Data;
using System.Data.SqlClient;
using System;

[assembly: OwinStartup(typeof(Startup))]

public class Startup
{
    public void Configuration(IAppBuilder app)
    {
        try
        {
            JobStorage.Current = new Hangfire.SqlServer.SqlServerStorage("DatabaseConnectionString");
            // Map Dashboard to the `http://<your-app>/hangfire` URL.
            app.UseHangfireDashboard();

            var options = new BackgroundJobServerOptions();
            //app.UseHangfireServer(options);
            
            //Add recurring job to scrape website
            String path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            RecurringJob.AddOrUpdate(() => HangFireJobs.Scrape.ScrapTimeTable(System.IO.Path.GetDirectoryName(path) + "\\"), Cron.Weekly);
        }
        catch (Exception) { }
    }
}