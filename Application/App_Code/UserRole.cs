﻿using System;
using System.Collections.Generic;
using System.Web.Security;

public class UserRole
{
    public string name { get; set; }

    public UserRole(String name)
    {
        this.name = name;
    }

    public static bool deleteRole(String role)
    {
        if (Roles.RoleExists(role))
        {
            try
            {

                return Roles.DeleteRole(role);
            }
            catch (Exception) { }
        }

        return false;
    }

    public static bool insertRole(String role)
    {
        if (!Roles.RoleExists(role))
        {
            try
            {
                Roles.CreateRole(role);
                return true;
            }
            catch (Exception) { }
        }

        return false;
    }
}

