﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Profile;
using System.Data;
using System.Security.Principal;


public class UserAccount
{
    public object userId { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string idNumber { get; set; }
    public string email { get; set; }
    public string password { get; set; }
    public string currentPassword { get; set; }
    public string [] role { get; set; }

    public static string STAFF = "staff";
    public static string ADMIN = "admin";
    public static string HR = "humanResource";
    public static string ACCOUNT = "account";

    public static string[] roles = { STAFF, ADMIN, HR, ACCOUNT };

    public static bool isUserAdmin()
    {
        return HttpContext.Current.User.IsInRole(ADMIN);
    }

    public static bool isUserStaff()
    {
        return HttpContext.Current.User.IsInRole(STAFF);
    }

    public static bool isUserHR()
    {
        return HttpContext.Current.User.IsInRole(HR);
    }

    public static bool isUserAccount()
    {
        return HttpContext.Current.User.IsInRole(ACCOUNT);
    }

    public static bool isUserElevated()
    {
        return isUserAdmin() || isUserHR() || isUserAccount();
    }

    public enum UserLevel { ZERO = 0, ONE, TWO, THREE, UNKNOWN  };

    public static UserLevel userLevel()
    {
        //if (isUserAdmin()) return UserLevel.THREE;
        if (isUserAccount()) return UserLevel.TWO;
        if (isUserHR()) return UserLevel.ONE;
        if (isUserStaff()) return UserLevel.ZERO;
        return UserLevel.UNKNOWN;
    }

    public Boolean registerUser(String defaultRole)
    {
        try
        {
            MembershipCreateStatus p;
            //Adds the new user to the database
            Membership.CreateUser(idNumber, password, email, "Nothing", "Nothing", true, out p);

            if (p == MembershipCreateStatus.Success)
            {
                //Add user to the default role (customer).
                Roles.AddUserToRole(idNumber, defaultRole);
                createUserProfile();
                return true;
            }
        }
        catch ( Exception)
        {
            //Nothing to do here
            //FlashHelper.FlashError(ex.Message);
        }

        return false;
    }

    public void createUserProfile()
    {
        ProfileBase profileBase = ProfileBase.Create(email);
        profileBase.SetPropertyValue("firstName", firstName);
        profileBase.SetPropertyValue("lastName", lastName);
        //profileBase.SetPropertyValue("idNumber", idNumber);
        profileBase.Save();
    }



    public static UserAccount getLoginUserInfo()
    {
        UserAccount user = new UserAccount();
        user.email = Membership.GetUser().Email;
        user.idNumber = Membership.GetUser().UserName;
        user.role = Roles.GetRolesForUser(Membership.GetUser().UserName);
        user.userId = Membership.GetUser().ProviderUserKey;
        ProfileBase profileBase = ProfileBase.Create(user.email);
        user.firstName = (String)profileBase.GetPropertyValue("firstName");
        user.lastName = (String)profileBase.GetPropertyValue("lastName");
        //user.idNumber = (String)profileBase.GetPropertyValue("idNumber");
        return user;
    }

    public static UserAccount getUserInfo(MembershipUser membershipUser)
    {
        UserAccount user = new UserAccount();
        user.idNumber = membershipUser.UserName;
        user.email = membershipUser.Email;
        user.userId = membershipUser.ProviderUserKey;
        user.role = Roles.GetRolesForUser(user.email);
        ProfileBase profileBase = ProfileBase.Create(user.email);
        user.firstName = (String)profileBase.GetPropertyValue("firstName");
        user.lastName = (String)profileBase.GetPropertyValue("lastName");
        //user.idNumber = (String)profileBase.GetPropertyValue("idNumber");
        return user;
    }

    public static UserAccount getUserInfo(Guid providerKey)
    {
        MembershipUser membershipUser = Membership.GetUser(providerKey);
        return getUserInfo(membershipUser);
    }

    public static UserAccount getUserInfoByEmail(string email)
    {
        string userName = Membership.GetUserNameByEmail(email);
        if (userName != null)
        {
            MembershipUser user = Membership.GetUser(userName);
            return getUserInfo(user);
        }

        return null;
    }

    public static UserAccount getUserInfoByUserName(string userName)
    {
        if (userName != null)
        {
            MembershipUser user = Membership.GetUser(userName);
            return getUserInfo(user);
        }

        return null;
    }

    public static IList<UserAccount> convertUserToIList(UserAccount user)
    {
        List<UserAccount> users = new List<UserAccount>(0);
        users.Add(user);
        return users;
    }
}