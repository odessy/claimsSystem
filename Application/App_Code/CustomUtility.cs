﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

public static class CustomUtility
{
    public static void standardErrorHandling(Exception ex)
    {
        if (ex != null)
        {
            FlashHelper.FlashError("A error has occurred. " +
                "Message: " + HttpUtility.HtmlEncode(String.Join(" ", Regex.Split(ex.Message, "\r\n"))));
        }
    }

    public static string formateTime(string time)
    {
        TimeSpan timeSpan = TimeSpan.Parse(time);
        int hours = timeSpan.Hours;
        int minutes = timeSpan.Minutes;
        string amPmDesignator = "AM";

        if (hours == 0)
        {
            hours = 12;
        }
        else if (hours == 12)
        {
            amPmDesignator = "PM";
        }
        else if (hours > 12)
        {
            hours -= 12;
            amPmDesignator = "PM";
        }

        return String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);
    }
}