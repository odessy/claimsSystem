﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

public static class FlashHelper
{
    public static void FlashInfo(string message)
    {
        FlashData.get().info.Add(message);
    }

    public static void FlashWarning(string message)
    {
        FlashData.get().warning.Add(message);
    }

    public static void FlashError(string message)
    {
        FlashData.get().error.Add(message);
    }

    public static string getHtmlMessage(List<String> messages, String className)
    {
        StringBuilder stringBuilder = new StringBuilder();
        if (messages != null)
        {
            foreach (String message in messages)
            {
                stringBuilder.Append("<div class=\" flash " + className + "\">" + message + "</div>");
            }
        }
        return stringBuilder.ToString();
    }

    public static string Flash(FlashData data)
    {
        StringBuilder messages = new StringBuilder();

        messages.Append(getHtmlMessage(data.info, "info"));
        messages.Append(getHtmlMessage(data.error, "error"));
        messages.Append(getHtmlMessage(data.warning, "warning"));

        StringBuilder sb = new StringBuilder();

        if (!String.IsNullOrEmpty(messages.ToString()))
        {
            sb.AppendFormat("<div id=\"FlashMessages\">{0}</div>", messages.ToString());
        }

        return sb.ToString();
    }
}