﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel;

/// <summary>
/// Summary description for ResponsibilityDB
/// </summary>
/// 

public class Responsibility 
{
    public int Id { get; set; }
    public int Hours { get; set; }
    public String Code { get; set; }
    public String Description { get; set; }
};

public class UserResponsibility
{
    public int Id { get; set; }
    public int FK_Responsibility { get; set; }
    public object FK_UserId { get; set; }
    public int FK_Semester_Id { get; set; }
}

public class ResponsibilityDB
{
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAll_Responsibilities()
    {
        return Connection.selectAllStoreProcedure("getAll_Responsibilities");
    }

    public static List<Responsibility> readResponsibilities(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<Responsibility> responsibilityList = new List<Responsibility>();

        if (dataTable == null) return responsibilityList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            Responsibility responsibility = new Responsibility();
            responsibility.Id = Convert.ToInt32(dataRow["Id"]);
            responsibility.Code = Convert.ToString(dataRow["Code"]);
            responsibility.Description = Convert.ToString(dataRow["Description"]);
            responsibility.Hours = Convert.ToInt32(dataRow["Hours"]);
            responsibilityList.Add(responsibility);
        }

        return responsibilityList;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertResposnsibility(Responsibility responsibility)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("insertResponsibility");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addVarCharParameter("@Code", responsibility.Code);
        con.addVarCharParameter("@Description", responsibility.Description);
        con.addIntParameter("@Hours", responsibility.Hours);

        int insert = 0;
        try
        {
            con.openConnection();
            insert =  (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }
        return insert; 
    }

    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int updateResponsibility(Responsibility responsibility)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("updateResponsibility");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@Id", responsibility.Id);
        con.addVarCharParameter("@Code", responsibility.Code);
        con.addVarCharParameter("@Description", responsibility.Description);
        con.addIntParameter("@Hours", responsibility.Hours);

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteResponsibility(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("deleteResponsibility", "@Id", Id, SqlDbType.Int);
    }

    //User Responsibility
    public static UserResponsibility _getUserResponsibility(int Id, object userId, int semester)
    {
        List<UserResponsibility> userResps = readUserResponsibility(getUserResponsibility(Id, userId, semester));

        if (userResps.Count > 0)
        {
            return userResps[0];
        }

        return null;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getUserResponsibility_By_Id(int Id)
    {
        return Connection.selectByKeyStoreProcedure("getUserResponsibility_By_Id", "@Id", Id, SqlDbType.Int);
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getUserResponsibility(int Id, object userId, int semester)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getUserResponsibility");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@FK_Responsibility", Id);
        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, userId);
        con.addIntParameter("@FK_Semester_Id", semester);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    public static List<UserResponsibility> readUserResponsibility(DataSet dataSet)
    {
        DataTable dataTable = dataSet.Tables[0];

        List<UserResponsibility> userResponsibilityList = new List<UserResponsibility>();

        if (dataTable == null) return userResponsibilityList;

        foreach (DataRow dataRow in dataTable.Rows)
        {
            UserResponsibility responsibility = new UserResponsibility();
            responsibility.Id = Convert.ToInt32(dataRow["Id"]);
            responsibility.FK_Responsibility = Convert.ToInt32(dataRow["FK_Responsibility"]);
            responsibility.FK_UserId = new Guid(Convert.ToString(dataRow["FK_UserId"]));
            responsibility.FK_Semester_Id = Convert.ToInt32(dataRow["FK_Semester_Id"]);
            userResponsibilityList.Add(responsibility);
        }

        return userResponsibilityList;
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet getAllUserResponsibilities_ByUserId(Guid userId, int semesterId)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand("getAll_Responsibilities_ByUserId");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, userId);
        con.addIntParameter("@SemesterId", semesterId);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static int insertUserResposnsibility(UserResponsibility responsibility)
    {
        Connection con = Connection.getConnection();
        con.setSqlCommand("insertUser_Responsibility");
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addIntParameter("@FK_Responsibility", responsibility.FK_Responsibility);
        con.addParameter("@FK_UserId", SqlDbType.UniqueIdentifier, responsibility.FK_UserId);
        con.addIntParameter("@FK_Semester_Id", responsibility.FK_Semester_Id);

        int insert = 0;
        try
        {
            con.openConnection();
            insert = (int)con.executeScalar();
        }
        finally
        {
            con.closeConnection();
        }
        return insert;
    }

    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static int deleteUserResponsibility(int Id)
    {
        return Connection.deleteByKeyStoreProcedure("deleteUserResponsibility", "@Id", Id, SqlDbType.Int);
    }

}