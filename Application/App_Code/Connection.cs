﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Connection class encapsulate the database interface objects
/// </summary>
public class Connection
{
    private SqlDataAdapter dataAdapter { get; set; }
    public DataSet dataSet { get; set; }
    private SqlCommand sqlCommand { get; set; }
    private SqlConnection sqlConnection { get; set; }
 
	public Connection()
	{
        dataAdapter = new SqlDataAdapter();
        dataSet = new DataSet();

        System.Configuration.Configuration rootWebConfig = WebConfigurationManager.OpenWebConfiguration("/");
        System.Configuration.ConnectionStringSettings connectionString;

        if (rootWebConfig.ConnectionStrings.ConnectionStrings.Count > 0)
        {
            connectionString = rootWebConfig.ConnectionStrings.ConnectionStrings["DatabaseConnectionString"];
            if (connectionString != null)
            {
                sqlConnection = new SqlConnection(connectionString.ConnectionString);
            }
            else
            {
                Console.WriteLine("No Databae connection string");
            }
        }
	}

    public static Connection getConnection()
    {
        return new Connection();
    }

    public void openConnection()
    {
        sqlConnection.Open();
    }

    public void setDataAdapterCommand()
    {
        dataAdapter.SelectCommand = sqlCommand;
    }

    public void dataAdapterFill()
    {
        dataAdapter.Fill(dataSet);
    }

    public int executeNonQuery()
    {
        return sqlCommand.ExecuteNonQuery();
    }

    public object executeScalar()
    {
        return sqlCommand.ExecuteScalar();
    }

    public void setSqlCommand(String procedure)
    {
        sqlCommand = new SqlCommand(procedure, sqlConnection);
    }

    public void setSqlCommandType(CommandType type)
    {
        sqlCommand.CommandType = type;
    }

    public void addParameter(SqlParameter parameter, object value)
    {
        sqlCommand.Parameters.Add(parameter).Value = value;
    }

    public void addParameter(String name, SqlDbType type, object value)
    {
        addParameter(new SqlParameter(name, type),  value);
    }

    public void addVarCharParameter(String name, object value)
    {
        addParameter(new SqlParameter(name, SqlDbType.VarChar), value);
    }

    public void addIntParameter(String name, object value)
    {
        addParameter(new SqlParameter(name, SqlDbType.Int), value);
    }

    public void closeConnection()
    {
        try
        {
            sqlConnection.Close();
        }
        catch (Exception){ }
    }

    public static DataSet selectAllStoreProcedure(string storeProcedure)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand(storeProcedure);
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    public static DataSet selectByKeyStoreProcedure(string storeProcedure, string key, object value, SqlDbType type)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand(storeProcedure);
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter(new SqlParameter(key, type), value);

        con.setDataAdapterCommand();

        try
        {
            con.openConnection();
            con.dataAdapterFill();
        }
        finally
        {
            con.closeConnection();
        }

        return con.dataSet;
    }

    public static int deleteByKeyStoreProcedure(string storeProcedure, string key, object value, SqlDbType type)
    {
        Connection con = Connection.getConnection();

        con.setSqlCommand(storeProcedure);
        con.setSqlCommandType(CommandType.StoredProcedure);

        con.addParameter(new SqlParameter(key, type), value);

        con.setDataAdapterCommand();

        int rowsAffected = 0;

        try
        {
            con.openConnection();
            rowsAffected = con.executeNonQuery();
        }
        finally
        {
            con.closeConnection();
        }

        return rowsAffected;
    }
}