﻿using System;
using System.Collections.Generic;
using System.Web;

public class FlashData
{
    List<String> _info;
    List<String> _warning;
    List<String> _error;

    public List<String> info 
    { 
        get
        {
            if (_info == null) _info = new List<string>();
            return _info;
        }
        set
        {
            _info = value;
        }
    }
    public List<String> warning 
    { 
        get
        {
            if (_warning == null) _warning = new List<string>();
            return _warning;
        }
        set
        {
            _warning = value;
        } 
    }
    public List<String> error
        { 
        get
        {
            if (_error == null) _error = new List<string>();
            return _error;
        }
        set
        {
            _error = value;
        } 
    }

    public static FlashData get()
    {
        FlashData data = (FlashData)HttpContext.Current.Session["Flash"];

        if (data == null)
        {
            HttpContext.Current.Session.Add("Flash", new FlashData());
        }

        return (FlashData)HttpContext.Current.Session["Flash"];
    }

    public void clear()
    {
        if (info != null) info.Clear();
        if (error != null) error.Clear();
        if (warning != null) warning.Clear();

        //remove flash from session
        HttpContext.Current.Session.Remove("Flash");
    }
}