﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DisplayLoginUser();
    }
    protected void Page_PreRender()
    {
        //Flash Messages Displaying
        String msg = FlashHelper.Flash(FlashData.get());
        FlashHolder.InnerHtml = msg;
        //clear the message after display
        FlashData.get().clear();
    }
    public void DisplayLoginUser()
    {
        UserAccount user = new UserAccount();
        user = UserAccount.getLoginUserInfo();

        lblUser.Text = user.firstName + " " + user.lastName;
    }
}
