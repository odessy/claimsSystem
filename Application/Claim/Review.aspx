﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Review.aspx.cs" Inherits="User_Review" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server">
    <ul class="breadcrumb">
        <li><asp:LinkButton ID="lbHome" PostBackUrl="~/Claim/Claim" runat="server" >Claim</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbSelection" PostBackUrl="~/Claim/TimeTable" runat="server" >Selection<span style="z-index: 99999; left: 10px; font-size: 16px;" class="showopacity glyphicon glyphicon-calendar"></span></asp:LinkButton></li>
        <li class="active"><asp:LinkButton ID="lbReview" runat="server" >Review</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbConfirmation" runat="server" >Confirmation</asp:LinkButton></li>
    </ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div id="aboutClaim">
        <span style="margin-right:20px;"><span class="text-bold">Name: </span><asp:Label runat="server" ID="lbName" ></asp:Label> </span>
        <span style="margin-right:20px;"><span class="text-bold">School: </span><asp:Label runat="server" ID="lbSchool" >SCIT</asp:Label> </span>
        <span><span class="text-bold">Period From: </span><asp:Label runat="server" ID="lbFrom" ></asp:Label><span class="text-bold"> To: </span><asp:Label runat="server" ID="lbTo" ></asp:Label> </span>
    </div>

    <div class="clearfix">
        <asp:ListView ID="lvClaimReview" runat="server">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th>Subject/Class</th>
                        <th>Schedule</th>
                        <th>Dates Worked</th>
                        <th>Total No. of Full-Time Hours</th>
                        <th>Total No. of OverTime/Part-Time Hours</th>
                        <th>Rate Per Hour</th>
                        <th>Amount</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbSubject" runat="server" Text='<%# Eval("ModuleCode") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbSchedule" runat="server" Text='<%# Eval("Day") + " " +Eval("TimeWorked") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbDatesWorked" runat="server" Text='<%# Eval("DatesWorked") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotalNumFullHours" runat="server" Text=' ' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotalNumOverHours" runat="server" Text='<%# Eval("TotalOH") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbRatePerHour" runat="server" Text='1000' />
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToDouble(Eval("TotalOH")) * 1000 %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <p>You currently have no claims in the system.</p>
                <script type="text/ecmascript">
                    $(document).ready(function () {
                        $("#aboutClaim").hide();
                    });
                </script>
            </EmptyDataTemplate>
        </asp:ListView>
        <table id="totalSummary" class="gridTable" style="margin-top: 10px; width: 300px; float: right;">
            <tr>
                <td>Total Claim</td>
                <td><asp:Label ID="lbTotalClaim" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Total Allowances</td>
                <td><asp:Label ID="lbTotalAllowances" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Total Taxable Claim</td>
                <td><asp:Label ID="lbTotalTaxableClaim" runat="server"></asp:Label></td>
            </tr>
        </table>

    </div>

    <br />

    <asp:LinkButton ID="btnBack" CausesValidation="true" CssClass="btn btn-primary btn-lg"
        runat="server" Text="Back" PostBackUrl="~/Claim/TimeTable.aspx" />
    <asp:LinkButton ID="btnContinue" CausesValidation="true" CssClass="btn btn-primary btn-lg"
        runat="server" Text="Continue" PostBackUrl="~/Claim/Confirmation" />

</asp:Content>
