﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TimeTable.aspx.cs" Inherits="User_TimeTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="<%: Request.ApplicationPath %>App_Themes/Default/Styles/fullcalendar.css" rel="stylesheet" />
    <link href="<%: Request.ApplicationPath %>App_Themes/Default/Styles/fullcalendar.print.css" rel="stylesheet" media="print" />
    <link href="<%: Request.ApplicationPath %>App_Themes/Default/Styles/jquery.timepicker.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentHead" Runat="Server">
    <ul class="breadcrumb">
        <li><asp:LinkButton ID="lbHome" PostBackUrl="~/Claim/Claim" runat="server" >Claim</asp:LinkButton></li>
        <li class="active"><asp:LinkButton ID="lbSelection" PostBackUrl="~/Claim/TimeTable" runat="server" >Selection<span style="z-index: 99999; left: 10px; font-size: 16px;" class="showopacity glyphicon glyphicon-calendar"></span></asp:LinkButton></li>
        <li><asp:LinkButton ID="lbReview" PostBackUrl="~/Claim/Review" runat="server" >Review</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbConfirmation" runat="server" >Confirmation</asp:LinkButton></li>
    </ul>
</asp:Content>

<asp:Content ContentPlaceHolderID="FooterScript" runat="server">
    <script src="<%: Request.ApplicationPath %>App_Themes/Default/Scripts/moment.js" type="text/javascript"></script>
    <script src="<%: Request.ApplicationPath %>App_Themes/Default/Scripts/fullcalendar.js" type="text/javascript"></script>
    <script src="<%: Request.ApplicationPath %>App_Themes/Default/Scripts/jquery.timepicker.js" type="text/javascript"></script>
    <script src="<%: Request.ApplicationPath %>App_Themes/Default/Scripts/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        var currentId = 0;
        var dateFormat = "YYYY MM DD hh:mma";
        var updateEvent = false;

        var calendar = null;

        $(document).ready(function () {

            // Manage status of dragging event and calendar
            var calEventStatus = [];
            var completed = false;

            $('#claimDurationStart').timepicker({
                'minTime': '8:00am',
                'maxTime': '9:00pm',
                'showDuration': false
            }).val("8:00am");;

            $('#claimDurationEnd').timepicker({
                'minTime': '8:30am',
                'maxTime': '9:00pm',
                'showDuration': false
            }).val("8:30am");


            /* Required functions */

            var isEventOverDiv = function (x, y) {

                var external_events = $('#external-events');
                var offset = external_events.offset();
                offset.right = external_events.width() + offset.left;
                offset.bottom = external_events.height() + offset.top;

                // Compare
                if (x >= offset.left
                    && y >= offset.top
                    && x <= offset.right
                    && y <= offset.bottom) { return true; }
                return false;

            }

            function makeEventsDraggable() {

                /* initialize the external events
                -----------------------------------------------------------------*/

                $('#external-events .fc-event').each(function () {

                    // store data so the calendar knows to render an event upon drop
                    $(this).data('eventObject', {
                        title: $.trim($(this).text()), // use the element's text as the event title
                        userRId: $.trim($(this).attr("data-user-event")),
                        hours: $.trim($(this).attr("data-hours"))
                    });

                    // make the event draggable using jQuery UI
                    $(this).draggable({
                        zIndex: 999,
                        revert: true,      // will cause the event to go back to its
                        revertDuration: 0  //  original position after the drag
                    });
                });
            }

            calendar = $('#timetable').fullCalendar({
                defaultView: 'agendaWeek',
                editable: true,
                selectable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                header: {
                    right: 'prev,next today',
                    center: 'title',
                    left: 'month,agendaWeek,agendaDay'
                },
                eventSources: [
                    {
                        url: '/Claim/TimeTableEvent?method=timeTableEvents', // use the `url` property
                        error: function () {
                            alert('there was an error while fetching events!');
                        },
                        editable: false,
                        durationEditable: false,
                        startEditable: false
                    },
                    {
                        url: '/Claim/TimeTableEvent?method=claimTimeTableEvents', // use the `url` property
                        error: function () {
                            alert('there was an error while fetching events!');
                        },
                        editable: true
                    }

                    // any other sources
                    // holiday sources

                ],
                drop: function (date, jsEvent, ui) {
                    //console.log('calendar 1 drop'); console.log(date); console.log(jsEvent); console.log(ui); console.log(this);

                    if (!dateAvailable(date)) return;

                    var eventObject = $(this).data('eventObject');
                    var copiedEventObject = copyEvent(eventObject);

                    copiedEventObject.start = date;

                    var secondsAdded = 3600;

                    if( eventObject.hours > 0 )
                    {
                        if (eventObject.hours >= secondsAdded) {
                            copiedEventObject.end = moment(date).add(secondsAdded, 'seconds');
                        }
                        else {
                            secondsAdded = eventObject.hours;
                            copiedEventObject.end = moment(date).add(eventObject.hours, 'seconds');
                        }
                    }
                    copiedEventObject.allDay = false;
                    copiedEventObject.color = "#FF530D";
                    copiedEventObject.type = "created";
                    copiedEventObject.id = "";

                    if (!loopEvents(calendar, copiedEventObject, isCovering)) {
                        if (secondsAdded == 3600) //1 hr try with 50 minutes
                        {
                            secondsAdded = 3000;
                            copiedEventObject.end = moment(date).add(secondsAdded, 'seconds');
                            if (!loopEvents(calendar, copiedEventObject, isCovering)) {
                                return;
                            }
                        }
                        else
                            return;
                    }

                    var found = false;

                    var events = getEventsByUserResponId(copiedEventObject.userRId);
                    if (events.length > 0) {
                        for (e in events) {
                            if (events[e].parentId == copiedEventObject.parentId ) {
                                //replace the event since the parent ids are the same
                                var a = getNormalizeDate(events[e]);
                                var ce = getNormalizeDate(copiedEventObject);

                                if (!(a.start > ce.end || a.end < ce.start)) {

                                    //merge since overlaping
                                    if ( copiedEventObject.start > events[e].start ) {
                                        copiedEventObject.start = events[e].start;
                                    }
                                    
                                    if (copiedEventObject.end > events[e].end) {
                                        copiedEventObject.end = copiedEventObject.end;
                                    }

                                    replaceEvent(events[e].id, copiedEventObject, true);

                                    found = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!found) {

                        AddEvent(copiedEventObject, function (data) {
                            console.log("AddEvent: " + data.status);
                            if (data.status == "ok") {
                                copiedEventObject.id = data.id;
                                // render the event on the calendar
                                // the last `true` argument determines if the event "sticks"
                                calendar.fullCalendar('renderEvent', copiedEventObject, false);
                            }
                        });
                    }
                },
                eventReceive: function (event) {
                    console.log('EventReceive');
                },
                eventRender: function (event, element, view) {
                    //console.log(event);
                    var theDate = moment(event.start).format("YYYY-MM-DD");
                    var startDate = event.recurstart;
                    var endDate = event.recurend;

                    if (startDate == null || endDate == null) {
                        return event.length;
                    }

                    if (startDate <= theDate && theDate <= endDate) {
                        //console.log(theDate);
                    }
                    else {
                        return event.length > 0;
                    }
                },
                eventDrop: function (event, delta, revertFunc) {
                    if (!loopEvents(calendar, event, isCovering) ||
                        !dateAvailable(event.start) ||
                        loopEvents(calendar, event, isOverlapping)) {
                        revertFunc();
                    }
                    else
                        ChangeModal("Update", AddEvent, event, function (data) { console.log("AddEvent: " + data.status); });
                },
                eventResize: function (event, delta, revertFunc) {
                    if (!loopEvents(calendar, event, isCovering) ||
                        !dateAvailable(event.start) ||
                        loopEvents(calendar, event, isOverlapping) ||
                        !rEventsResize(event, delta)) {
                        revertFunc();
                    }
                    else
                        ChangeModal("Update", AddEvent, event, function (data) { console.log("AddEvent: " + data.status); });

                },
                eventDragStop: function (event, jsEvent, ui, view) {

                    if (isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
                        ChangeModal("Delete", RemoveEvent, event, function (data) {
                            console.log("RemoveEvent: " + data.status);
                            if (data.status == "ok") {
                                //var el = $("<div class='fc-event'>").appendTo('#external-events-listing').text(event.title);
                                calendar.fullCalendar('removeEvents', event._id);
                                //el.draggable({
                                //    zIndex: 999,
                                //    revert: true,
                                //    revertDuration: 0
                                //});
                            }
                        });
                        makeEventsDraggable();
                    }
                },
                //header and other values
                select: function (start, end, allDay) {

                    if (!dateAvailable(start)) return;

                    // Ensure that the event will be covered by timetable event
                    var eventObj = {};
                    eventObj.id = "";
                    eventObj.start = start;
                    eventObj.end = end;

                    if (!loopEvents(calendar, eventObj, isCovering)) {
                        return;
                    }

                    starttime = moment(start)
                        //.subtract(4, 'h')
                        .format(dateFormat);
                    endtime = starttime;//moment(end).format(dateFormat);
                    var mywhen = moment(start).format("MMMM Do YYYY");
                    $('#createEventModal #apptStartTime').val(starttime);
                    $('#createEventModal #apptEndTime').val(endtime);
                    $('#createEventModal #apptAllDay').val(allDay);
                    $('#createEventModal #when').text(mywhen);
                    //remove the update event value
                    $('#createEventModal #updateEvent').val("");
                    $('#createEventModal').modal('show');
                },
                eventClick: function (event, element) {

                    if (!dateAvailable(event.start)) return;

                    if(event.type != "created") return;

                    starttime = moment(event.start).format(dateFormat);
                    endtime = moment(event.end).format(dateFormat);
                    var mywhen = moment(event.start).format("MMMM Do YYYY");
                    $('#createEventModal #claimDurationStart').val(moment(event.start).format("hh:mma"));
                    $('#createEventModal #claimDurationEnd').val(moment(event.end).format("hh:mma"));
                    $('#createEventModal #apptStartTime').val(starttime);
                    $('#createEventModal #apptEndTime').val(endtime);
                    $('#createEventModal #apptAllDay').val(event.allDay);
                    $('#createEventModal #when').text(mywhen);//+ " " + ((moment(event.end) - moment(event.start)) / 1000) / 60 + " mins");
                    if(event.type == "created" )$('#createEventModal #updateEvent').val(event.id);
                    $('#createEventModal #updateEventType').val(event.type);
                    $('#createEventModal').modal('show');
                },
                viewRender: function (view, element) {
                    var now = new Date();
                    var end = new Date();
                    //end.setMonth(now.getMonth() + 1); //Adjust as needed
                    now.setMonth(now.getMonth() - 13); //Adjust as needed

                    var cal_date = view.start;
                    var cur_date = moment(now);
                    var end_date = moment(end);

                    //console.log(cal_date); console.log(cur_date); console.log(end_date);

                    if (cal_date <= cur_date) { $('.fc-prev-button').addClass("fc-state-disabled"); }
                    else { $('.fc-prev-button').removeClass("fc-state-disabled"); }

                    if (cal_date >= end_date) { $('.fc-next-button').addClass("fc-state-disabled"); }
                    else { $('.fc-next-button').removeClass("fc-state-disabled"); }

                    if(calendar != null) updateREvents();
                },
                eventAfterAllRender: function (view) {
                    updateREvents();
                    $("#sidebar").show();
                    if (!completed) {
                        var events_array = calendar.fullCalendar('clientEvents');
                        calendar.fullCalendar('gotoDate', moment(events_array[0].recurstart));
                        completed = true;
                    }
                }
            });

            $('#cancelButton').on('click', function (e) {
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                $('#updateEvent').val("");

            });

            $('.btn-update-ok').on('click', function (e) {
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                doSubmit(false);

            });

            $('.btn-update-all').on('click', function (e) {
                // We don't want this to act as a link so cancel the link action
                e.preventDefault();
                doSubmit(true);

            });

            function copyEvent(event) {
                var other = {};
                other.id = event.id;
                other.userRId = event.userRId;
                other.start = event.start;
                other.end = event.end;
                other.type = event.type;
                other.parentId = event.parentId;
                other.title = event.title;
                other.hours = event.hours;
                return other;
            }

            function clickFun(args) {
                args.data[0](args.data[1], args.data[2], args.data[3]);
                $('#confirm-update').modal('hide');
                //unbind both events
                $('#confirm-update .btn-ok').unbind('click');
                $('#confirm-update .btn-all').unbind('click');
            }

            function ChangeModal(text, update, event, success) {
                $('#confirm-update .btn-ok').text(text);
                $('#confirm-update').modal('show');
                $('#confirm-update .btn-ok')
                    .click([update, event, success, false], clickFun);
                $('#confirm-update .btn-all')
                    .click([update, event, success, true], clickFun);
            }

            function RemoveEvent(event, success, all) {
                $.ajax({
                    url: '/Claim/TimeTableEvent?method=removeEvent&eventId=' + event.id + "&all="+all,
                    dataType: 'json',
                    async: false,
                    error: function (xhr, ajaxOptions, thrownError) {
                        //console.log(xhr.status);
                        //console.log(thrownError);
                    },
                    success: success
                });

                updateREvents();
            }

            function AddEvent(event, success, all) {
                var copyToSend = copyEvent(event);
                copyToSend.start = copyToSend.start.format(dateFormat)
                copyToSend.end = copyToSend.end.format(dateFormat);

                if (typeof all == "undefined") all = true;

                $.ajax({
                    method: "POST",
                    url: '/Claim/TimeTableEvent?method=addEvent' + "&all=" + all,
                    dataType: 'json',
                    async: false,
                    data: copyToSend,
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        //console.log(xhr.status);
                        //console.log(thrownError);
                    },
                    success: success
                });

                updateREvents();
            }

            function dateAvailable(date) {
                var selectedDate = moment(date).toDate();
                var today = new Date();
                var oneYearAgo = new Date();

                oneYearAgo.setFullYear(today.getFullYear() - 1);

                if (selectedDate > today || selectedDate < oneYearAgo) {
                    // So it will be unselectable
                    return false;
                }

                return true;
            }

            function getNormalizeDate(event) {
                var object = {};
                object.start = new moment(event.start.format(dateFormat), dateFormat);
                object.end = new moment(event.end.format(dateFormat), dateFormat);
                return object;
            }

            function loopEvents(calendar, event, Function) {
                var array = getEvents();
                for (i in array) {
                    if (Function(array[i], event)) {
                        return true;
                    }
                }
                return false;
            }

            function isCovering(array, event) {
                if ((array.id != event.id) &&
                    array.start.format("YYYY MM DD") == moment(event.start).format("YYYY MM DD") &&
                    (array.type != "created")) {

                    //ensure all formats are correct before comparison

                    var a = getNormalizeDate(array);
                    var e = getNormalizeDate(event);

                    if (a.start <= e.start
                        && a.end >= e.end) {
                        //set event parent id
                        event.parentId = array.id;
                        return true;
                    }

                }
                return false;
            }

            function rEventsResize(event, delta) {
                var result = false;
                $('#external-events .fc-event').each(function () {
                    var userRId = $.trim($(this).attr("data-user-event"));
                    var hours = parseFloat($.trim($(this).attr("data-hours")));

                    if (userRId == event.userRId)
                    {
                        if (delta._milliseconds < 0)
                            result = true;
                        else {
                            //hours here changed to seconds
                            if (hours >= ((delta._milliseconds / 1000))) {
                                result = true;
                            }
                        }
                    }
                });

                return result;
            }

            function updateREvents() {
                $('#external-events .fc-event').each(function () {
                    var title =  $.trim($(this).text());
                    var userRId = $.trim($(this).attr("data-user-event"));
                    var hours = parseFloat($.trim($(this).attr("data-hours")));
                    var orgHours = parseFloat($.trim($(this).attr("data-org-hours")));

                    var events = getEventsByUserResponId(userRId);
                    var sum = 0;

                    for (e in events) {
                        sum += (((events[e].end - events[e].start) / 1000));
                    }

                    $(this).attr("data-hours", orgHours - sum );

                    if ($.trim($(this).attr("data-hours")) == "0")
                        $(this).hide();
                    else
                        $(this).show();

                });

                //recheck events
                makeEventsDraggable();
            }

            function isOverlapping(array, event) {
                if ((array.id != event.id) &&
                    array.start.format("YYYY MM DD") == moment(event.start).format("YYYY MM DD") &&
                    (array.type == "created")) {

                    //ensure all formats are correct before comparison

                    var a = getNormalizeDate(array);
                    var e = getNormalizeDate(event);

                    if ( !(a.start >= e.end
                        || a.end <= e.start) ) {

                        return true;
                    }
                }
                return false;
            }

            function getEventsByUserResponId(Id) {
                var array = getEvents();
                var events = [];
                for (i in array) {
                    if (array[i].userRId == Id) {
                        events.push(array[i]);
                    }
                }
                return events;
            }

            function getCalendarDateRange() {
                var view = calendar.fullCalendar('getCalendar').view;
                var start = view.start._d;
                var end = view.end._d;
                var dates = { start: start, end: end };
                return dates;
            }

            function sortDates(array, dateRange) {
                var newA = [];
                for (i in array) {
                    if (!(array[i].start < dateRange.start ||
                        array[i].end > dateRange.end || 
                        array[i].start > dateRange.end ||
                        array[i].end < dateRange.start)) {
                        newA.push(array[i]);
                    }
                }
                return newA;
            }

            function getEvents() {
                var array = calendar.fullCalendar('clientEvents');
                return sortDates(array, getCalendarDateRange());
            }

            function updateEventFun(eventId, eventObj, newEvent, all) {
                RemoveEvent(eventObj, function (data) {
                    console.log("RemoveEvent: " + data.status);
                    if (data.status == "ok") {
                        calendar.fullCalendar('removeEvents', eventId);

                        eventObj.color = newEvent.color;
                        eventObj.type = newEvent.type;
                        //eventObj.title = newEvent.title;
                        eventObj.start = newEvent.start;
                        eventObj.end = newEvent.end;
                        eventObj.allDay = newEvent.allDay;
                        eventObj.parentId = newEvent.parentId;
                        eventObj.id = newEvent.id;

                        AddEvent(eventObj, function (data) {
                            console.log("AddEvent: " + data.status);
                            if (data.status == "ok") {
                                eventObj.id = data.id;
                                calendar.fullCalendar('renderEvent', eventObj);
                            }
                        }, all);

                        calendar.fullCalendar('refresh');
                    }
                }, all);
            }

            function replaceEvent(eventId, newEvent, all) {
                var events = calendar.fullCalendar('clientEvents', [eventId])
                var eventObj = {};

                if (typeof all == "undefined") all = true;

                if (events.length > 0) {
                    eventObj = copyEvent(events[0]);

                    var delta = {};

                    delta._milliseconds = (newEvent.end - newEvent.start) - (events[0].end - events[0].start);

                    if (!rEventsResize(events[0], delta)) return;

                    updateEventFun(eventId, eventObj, newEvent, all);
                }
            }

            function doSubmit(all) {
                $("#createEventModal").modal('hide');

                var eventObj = {};
                var startTime = $("#claimDurationStart").val();
                var endTime = $("#claimDurationEnd").val();
                var momentDate = new moment($('#apptStartTime').val(), "YYYY MM DD").format("YYYY MM DD");

                var momentStartDate = new moment(momentDate + " " + startTime, dateFormat);
                var momentEndDate = new moment(momentDate + " " + endTime, dateFormat);

                if (!momentEndDate.isAfter(momentStartDate)) {
                    return;
                }

                var eventId = $('#updateEvent').val();

                eventObj.color = "#FF530D";
                eventObj.type = "created";
                eventObj.start = momentStartDate;
                eventObj.end = momentEndDate;
                eventObj.allDay = false;//($('#apptAllDay').val() == "true");
                eventObj.parentId = "";
                eventObj.id = eventId;

                if (loopEvents(calendar, eventObj, isCovering)
                    && !loopEvents(calendar, eventObj, isOverlapping)
                    //&& dateAvailable(eventObj.start)
                    ) {

                    //var eventType = $('#updateEventType').val();
                    replaceEvent(eventId, eventObj, all);
                    console.log(all);
                }
            }
        });
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <!--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#createEventModal">Launch demo modal</button>-->
    <div id="timetable" style="min-width:800px; max-width: 74%; float: left;"></div>

    <div id="sidebar" class="clearfix" style="display:none; margin-left: 15px; width: 200px; float: left;">
        <div class="clearfix" style=" text-align:left; padding-bottom: 5px;" >
            <asp:LinkButton ID="btnBack" CausesValidation="false" CssClass="btn btn-primary btn-lg"
                runat="server" Text="Back"  PostBackUrl="~/Claim/Claim" />
            <asp:LinkButton ID="btnContinue" CausesValidation="false" CssClass="btn btn-primary btn-lg"
                runat="server" Text="Continue" PostBackUrl="~/Claim/Review" />
        </div>
        <div id='external-events' class="clearfix"  style="width: 100%" >
            <div id="external-events-listing" class="clearfix" >
                <h4>Responsibility Events</h4>
                <p id="calendarTrash" style="padding-top: 5px; padding-right: 5px; padding-left: 5px;">
                    <span class="glyphicon glyphicon-trash"></span>Drag Here To Remove
                </p>
                <asp:Repeater runat="server" ID="eventRepeater">
                    <ItemTemplate>
                        <div class='fc-event' data-user-event='<%# DataBinder.Eval(Container.DataItem, "Id" ) %>' data-org-hours='<%# 3600 * Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Hours" )) %>' data-hours='<%# 3600 * Convert.ToInt32(DataBinder.Eval(Container.DataItem, "Hours" )) %>' id='<%# DataBinder.Eval(Container.DataItem, "FK_Responsibility" ) %>'><%# DataBinder.Eval(Container.DataItem, "Description" ) %></div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div class="legend" >
            <div class="clearfix" style="width: 90%; margin: 0 auto;">
                <h4>Legend</h4>
                <div class="clearfix" style="width: 90%">
                    <span style="float: left">Practical Class</span>
                    <span style="background-color: #000; margin-top:6px; padding: 5px; float: right;"></span>
                </div>
                <div class="clearfix" style="width: 90%">
                    <span style="float: left">Other Class </span>
                    <span style="background-color: #3a87ad; margin-top:6px; padding: 5px; float: right;"></span>
                </div>
                <div class="clearfix" style="width: 90%">
                    <span style="float: left">Claim Event </span>
                    <span style="background-color: #ff530d; margin-top:6px; padding: 5px; float: right;"></span>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="createEventModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3 class="modal-title" id="myModalLabel">Claim for <span class="controls controls-row" id="when" style="margin-top: 5px;"></span></h3>
                </div>
                <div class="modal-body form">
                    <div class="control-group">
                        <div class="controls">
                            <input type="hidden" id="apptStartTime" />
                            <input type="hidden" id="apptEndTime" />
                            <input type="hidden" id="apptAllDay" />
                            <input type="hidden" id="updateEvent" />
                            <input type="hidden" id="updateEventType" />
                        </div>
                    </div>
                    <div class="control-div  clearfix">
                        <span class="control-label">Start Time</span>
                        <div class="col-sm-10">
                            <input type="text" id="claimDurationStart" />
                        </div>
                    </div>
                    <div class="control-div  clearfix">
                        <span class="control-label">End Time</span>
                        <div class="col-sm-10">
                            <input type="text" id="claimDurationEnd" />
                        </div>
                    </div>
<%--                    <div class="control-div  clearfix">
                        <span class="control-label">Comment</span>
                        <div class="col-sm-10">
                            <input type="text" id="comments" />
                        </div>
                    </div>--%>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" id="cancelButton" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <%--<button type="button" class="btn btn-primary" id="submitButton">Save</button>--%>
                        <button type="button"  class="btn btn-warning btn-update-ok">Update</button>
                        <button type="button"  class="btn btn-danger btn-update-all">All</button>
                    </div>
                </div>
            </div>
            <!--/.modal-content-->
        </div>
        <!--/.modal-dialog -->
    </div>

    <div class="modal fade" id="confirm-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    Select the option to make change to this single event or all?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button"  class="btn btn-warning btn-ok">Delete</button>
                    <button type="button"  class="btn btn-danger btn-all">All</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

