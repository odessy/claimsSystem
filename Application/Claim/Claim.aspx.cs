﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Claim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        if (rbList.SelectedValue.Trim() == "Yes")
            Response.Redirect("~/Claim/TimeTable.aspx");
        else
            Response.Redirect("~/Claim/Review.aspx");
    }
}