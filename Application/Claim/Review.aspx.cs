﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Review : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getClaimDates();
    }

    protected void updateClaim(List<ClaimSummary2> claimSummaryList, Claim claim)
    {
        lvClaimReview.DataSource = claimSummaryList;
        lvClaimReview.DataBind();

        //update the claim in the system
        ClaimDB.updateClaim(claim);

        lbTotalClaim.Text = Convert.ToString(claim.Total_Claim);
        lbTotalAllowances.Text = Convert.ToString(claim.Total_Allowances);
        lbTotalTaxableClaim.Text = Convert.ToString(claim.Total_Taxable_Claim);
    }

    /**
     * TODO: Increase the efficiency
     * */
    protected void getClaimDates()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        List<TimeTable> list = TimeTableDB.readTimeTables(TimeTableDB.getAllTimeTable_ByName(user.firstName, user.lastName));
        if (list.Count > 0)
        {
            List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
            List<ClaimTimeTableEvent> clist = new List<ClaimTimeTableEvent>(0);
            if (claims.Count > 0)
            {

                claims[0].Total_Claim = 0;
                claims[0].Total_Minutes = 0;

                lbName.Text = user.firstName +" "+ user.lastName;
                lbFrom.Text = claims[0].Period_From.ToString("MM/dd/yyyy");
                lbTo.Text = claims[0].Period_To.ToString("MM/dd/yyyy");

                updateClaim(ClaimDB.getClaimSummary(claims[0]), claims[0]);
            }
        }
    }
}