﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ClaimView.aspx.cs" Inherits="User_ClaimView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server">
    <h1 class="page-title">Claim View Page</h1>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="alert alert-help prepend-top-default">Claim Summary</div>
    <div id="aboutClaim">
        <span style="margin-right: 20px;"><span class="text-bold">Name: </span>
            <asp:Label runat="server" ID="lbName"></asp:Label>
        </span>
        <span style="margin-right: 20px;"><span class="text-bold">School: </span>
            <asp:Label runat="server" ID="lbSchool">SCIT</asp:Label>
        </span>
        <span><span class="text-bold">Period From: </span>
            <asp:Label runat="server" ID="lbFrom"></asp:Label><span class="text-bold"> To: </span>
            <asp:Label runat="server" ID="lbTo"></asp:Label>
        </span>
    </div>

    <div class="clearfix">
        <asp:ListView ID="lvClaimReview" runat="server">
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th>Subject/Class</th>
                        <th>Schedule</th>
                        <th>Dates Worked</th>
                        <th>Total No. of Full-Time Hours</th>
                        <th>Total No. of OverTime/Part-Time Hours</th>
                        <th>Rate Per Hour</th>
                        <th>Amount</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbSubject" runat="server" Text='<%# Eval("ModuleCode") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbSchedule" runat="server" Text='<%# Eval("Day") + " " +Eval("TimeWorked") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbDatesWorked" runat="server" Text='<%# Eval("DatesWorked") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotalNumFullHours" runat="server" Text=' ' />
                    </td>
                    <td>
                        <asp:Label ID="lbTotalNumOverHours" runat="server" Text='<%# Eval("TotalOH") %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbRatePerHour" runat="server" Text='1000' />
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToDouble(Eval("TotalOH")) * 1000 %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <p>You currently have no claims in the system.</p>
                <script type="text/ecmascript">
                    $(document).ready(function () {
                        $("#aboutClaim").hide();
                    });
                </script>
            </EmptyDataTemplate>
        </asp:ListView>
        <table id="totalSummary" class="gridTable" style="margin-top: 10px; width: 300px; float: right;">
            <tr>
                <td>Total Claim</td>
                <td>
                    <asp:Label ID="lbTotalClaim" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Total Allowances</td>
                <td>
                    <asp:Label ID="lbTotalAllowances" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Total Taxable Claim</td>
                <td>
                    <asp:Label ID="lbTotalTaxableClaim" runat="server"></asp:Label></td>
            </tr>
        </table>
    </div>


    <% if (UserAccount.isUserElevated())
       { %>
    <div style="margin-top: 10px;" class="alert alert-help prepend-top-default">Review the claim</div>
    <div class="form">
        <div class="control-div clearfix">
            <asp:Label runat="server">The information in the claim summary form above is correct and applicable to the lecturer stated in the claim.</asp:Label><br />
            <asp:RadioButton ID="rbVerifiedYes" runat="server" Checked="true" GroupName="rbVerified" Text="Yes" /><br />
            <asp:RadioButton ID="rbVerifiedNo" runat="server" GroupName="rbVerified" Text="No" />
        </div>
        <div style="display: none;" runat="server" id="divMessage" class="control-div clearfix">
            <asp:Label ID="lbMessage" runat="server">Please specify a reason below:</asp:Label>
            <asp:TextBox ID="tbMessage" Style="width: 100%; height: 150px;" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
        <div class="form-group clearfix">
            <asp:Button ID="btnSubmit" CausesValidation="true" CssClass="btn btn-danger btn-lg"
                runat="server" OnClick="btnSubmit_Click" Text="Submit" />
        </div>
    </div>
    <% } %>

    <div>

    <asp:ListView ID="lvMessage" runat="server" DataKeyNames="Id"
        OnItemCommand="lvMessage_ItemCommand">
        <LayoutTemplate>
            <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                <tr id="Tr2" runat="server" style="">
                    <th id="Th1" runat="server">Subject</th>
                    <th id="Th2" runat="server">From</th>
                    <th id="Th3" runat="server">Message</th>
                    <th id="Th4" runat="server">Date</th>
                </tr>
                <tr id="itemPlaceholder" runat="server">
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr style="">
                <td>
                    <asp:Label ID="lbSubject" runat="server" Text='<%# Eval("Subject").ToString() %>' />
                </td>
                <td>
                    <asp:Label ID="lbClaimId" runat="server" Text='<%# getUsername(Eval("FK_From_UserId").ToString())%>' />
                </td>
                <td>
                    <asp:Label ID="lbMessage" runat="server" Text='<%# Eval("Message").ToString() %>' />
                </td>
                <td>
                    <asp:Label ID="lbDate" runat="server" Text='<%# Eval("Recieved_Date").ToString() %>' />
                </td>

            </tr>
        </ItemTemplate>
    </asp:ListView>

    </div>

    <div style="margin-bottom: 10px; margin-top: 10px;">
        <asp:LinkButton ID="btnBack" CausesValidation="false" CssClass="btn btn-primary btn-lg"
            runat="server" Text="Back" PostBackUrl="~/User/Home.aspx" />
    </div>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="FooterScript" runat="server">
    <% if(UserAccount.isUserElevated()) { %>
    <script type="text/ecmascript">
        $(document).ready(function () {

            function isYes(element) {
                if ($(element).is(':checked')) {
                    $('#<%= divMessage.ClientID %>').hide();
                }
            }

            function isNo(element) {
                if ($(element).is(':checked')) {
                    $('#<%= divMessage.ClientID %>').show();
                }
            }

            $('#<%= rbVerifiedYes.ClientID %>').change(function () {
                isYes(this);
            });

            $('#<%= rbVerifiedNo.ClientID %>').change(function () {
                isNo(this);
            });

            isYes('#<%= rbVerifiedYes.ClientID %>');
            isNo('#<%= rbVerifiedNo.ClientID %>');
        });
    </script>
    <% } %>
</asp:Content>
