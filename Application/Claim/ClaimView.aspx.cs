﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ClaimView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                string claimIdStr = Request.QueryString["claimId"].Trim();
                int claimId = Convert.ToInt32(claimIdStr);
                getClaimDates(claimId);
                btnSubmit.CommandArgument = claimIdStr;

                BindList(claimId);

            }
            catch (Exception)
            {
                FlashHelper.FlashError("Invalid claim Id given.");
            }
        }
    }

    protected void updateClaim(List<ClaimSummary2> claimSummaryList, Claim claim)
    {
        lvClaimReview.DataSource = claimSummaryList;
        lvClaimReview.DataBind();

        lbTotalClaim.Text = Convert.ToString(claim.Total_Claim);
        lbTotalAllowances.Text = Convert.ToString(claim.Total_Allowances);
        lbTotalTaxableClaim.Text = Convert.ToString(claim.Total_Taxable_Claim);
    }

    /**
     * TODO: Increase the efficiency
     * */
    protected void getClaimDates(int claimId)
    {

        List<Claim> claims = ClaimDB.readClaims(ClaimDB.getClaim_ById(claimId));
        List<ClaimTimeTableEvent> clist = new List<ClaimTimeTableEvent>(0);
        if (claims.Count > 0)
        {

            UserAccount user = UserAccount.getUserInfo((Guid)claims[0].FK_UserId);

            if (UserAccount.isUserStaff())
            {
                if ((Guid)claims[0].FK_UserId != (Guid)UserAccount.getLoginUserInfo().userId)
                {
                    FlashHelper.FlashError("You don't have the required permission to view this claim");
                    return;
                }
            }
            else if (claims[0].Status == 0 || claims[0].Status > (int)UserAccount.userLevel()) //no permission to change claim.
            {
                FlashHelper.FlashError("You don't have the required permission to view this claim");
                return;
            }

            claims[0].Total_Claim = 0;
            claims[0].Total_Minutes = 0;

            lbName.Text = user.firstName + " " + user.lastName;
            lbFrom.Text = claims[0].Period_From.ToString("MM/dd/yyyy");
            lbTo.Text = claims[0].Period_To.ToString("MM/dd/yyyy");

            updateClaim(ClaimDB.getClaimSummary(claims[0]), claims[0]);
        }
        else
            FlashHelper.FlashError("Claim not found");
    }


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Button button = (Button)sender;
            int claimId = Convert.ToInt32(button.CommandArgument);

            List<Claim> claims = ClaimDB.readClaims(ClaimDB.getClaim_ById(claimId));
            if (claims.Count > 0)
            {
                if (rbVerifiedYes.Checked)
                {
                    claims[0].Status += 1; //elevate the status to next;
                }
                else if (rbVerifiedNo.Checked)
                {
                    claims[0].Status = 4;//-= 1; //reduce the status

                    if(!String.IsNullOrEmpty(tbMessage.Text.Trim()))
                    {
                        //send message to the user
                        MessageObj msg = new MessageObj();
                        msg.Message = tbMessage.Text;
                        msg.To_UserId = claims[0].FK_UserId;
                        msg.From_UserId = UserAccount.getLoginUserInfo().userId;
                        msg.Subject = "Claim Error";
                        msg.Received_Date = DateTime.Now;
                        //Insert the message into the database
                        int msgId = MessageDB.insertMessage(msg);
                        //add claim review
                        ClaimReview review = new ClaimReview();
                        review.FK_Claim_Id = claimId;
                        review.FK_Message_Id = msgId;
                        ClaimDB.insertClaimReview(review);
                    }
                    else
                    {
                        FlashHelper.FlashError("Please specify a message");
                        return;
                    }
                }

                ClaimDB.updateClaim(claims[0]);

                FlashHelper.FlashInfo("The claim was reviewed.");

                Response.Redirect("~/User/Home.aspx");
            }
            else
            {
                FlashHelper.FlashError("Unable to find claim");
            }
        }
        catch (Exception)
        {

        }
    }
    protected String getUsername(String userId)
    {
        try
        {
            UserAccount user = UserAccount.getUserInfo(new Guid(userId));
            return user.firstName + " " + user.lastName;
        }
        catch (Exception)
        {
        }

        return "";
    }
    protected void BindList(int claimId)
    {
        lvMessage.DataSource = MessageDB.getClaimReview_ById(claimId);
        lvMessage.DataBind();
    }
    protected bool isInbox(string userId)
    {
        return (Guid)UserAccount.getLoginUserInfo().userId != new Guid(userId);
    }
    protected void lvMessage_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "Read")
        {
            MessageObj message = new MessageObj();
            List<MessageObj> messages = MessageDB.readMessage(MessageDB.getAllMessage_ById(Convert.ToInt32(e.CommandArgument.ToString())));
            if (messages.Count > 0)
                message = messages[0];

          
            if (!isInbox(message.From_UserId.ToString()))
            {
                message.Status = 1;
                MessageDB.updateMessageStatus(message);
            }

        }
    }
}