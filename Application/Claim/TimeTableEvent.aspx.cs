﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_TimeTableEvent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string method = Request.QueryString["method"];
        if (method != null)
        {
            if (method.Trim() == "addEvent") addEvent();
            else if (method.Trim() == "removeEvent") removeEvent();
            else if (method.Trim() == "timeTableEvents") timeTableEvents();
            else if (method.Trim() == "claimTimeTableEvents") claimTimeTableEvents();
            return;
        }
    }

    public void timeTableEvents()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        List<TimeTable> list = TimeTableDB.readTimeTables(TimeTableDB.getAllTimeTable_ByName(user.firstName, user.lastName));
        if (list.Count > 0)
        {
            List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
            List<ClaimTimeTableEvent> clist = new List<ClaimTimeTableEvent>(0);

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write("[");

            if (claims.Count > 0)
            {
                clist = ClaimDB.readClaimTimeTableEvents(ClaimDB.getAll_ClaimTimeTableEvents_Claim_Id(claims[0].Id));

                Semester semester = TimeTableDB.readSemester(TimeTableDB.getSemester_ById(list[0].FK_Semester_Id))[0];
                List<TimeTableEvent> events = TimeTableDB.readTimeTableEvents(TimeTableDB.getAllTimeTableEvents(list[0].Id));

                Response.Write(timeTableEvent(events, semester, claims[0]));
            }

            Response.Write("]");
            Response.End();

        }
    }

    public void claimTimeTableEvents()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        List<TimeTable> list = TimeTableDB.readTimeTables(TimeTableDB.getAllTimeTable_ByName(user.firstName, user.lastName));
        if (list.Count > 0)
        {
            List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
            List<ClaimTimeTableEvent> clist = new List<ClaimTimeTableEvent>(0);

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.Write("[");

            if (claims.Count > 0)
            {
                clist = ClaimDB.readClaimTimeTableEvents(ClaimDB.getAll_ClaimTimeTableEvents_Claim_Id(claims[0].Id));
                Semester semester = TimeTableDB.readSemester(TimeTableDB.getSemester_ById(list[0].FK_Semester_Id))[0];
                List<TimeTableEvent> events = TimeTableDB.readTimeTableEvents(TimeTableDB.getAllTimeTableEvents(list[0].Id));
                Response.Write(claimTimeTableEvent(clist));
            }

            Response.Write("]");
            Response.End();
        }
    }

    public String timeTableEvent(List<TimeTableEvent> events, Semester semester, Claim claim)
    {
        StringBuilder builder = new StringBuilder();

        int count = 0;

        foreach (TimeTableEvent @event in events)
        {
            builder.Append("{");
            builder.Append("\"allDay\" : false,");
            builder.Append("\"id\" : \"" + @event.Id + "\",");
            builder.Append("\"color\" :\" " + getColor(@event.FK_Category_Id) + "\",");
            builder.Append("\"title\" : \"" + getModule(@event.FK_Module_Id) + "\",");
            builder.Append("\"start\": " + "\"" + getDateTime(claim.Period_From, @event.Day, @event.Start_Time) + "\",");
            builder.Append("\"end\": " + "\"" + getDateTime(claim.Period_To, @event.Day, @event.End_Time) + "\",");
            builder.Append("\"dow\" : \"[" + (@event.Day + 1) + "]\",");
            builder.Append("\"recurstart\" : \"" + claim.Period_From.ToString("yyyy-MM-dd") + "\",");
            builder.Append("\"recurend\" : \"" + claim.Period_To.ToString("yyyy-MM-dd") + "\",");
            builder.Append("\"type\" : \"timetable\",");
            builder.Append("\"editable\" :\"false\"");
            builder.Append("}");
            if (count < events.Count - 1)
                builder.Append(",");
            count++;
        }

        return builder.ToString();
    }

    public String claimTimeTableEvent(List<ClaimTimeTableEvent> clist)
    {
        StringBuilder builder = new StringBuilder();

        //foreach (TimeTableEvent  @event in events)
        //{
            //List<ClaimTimeTableEvent> clist = ClaimDB.readClaimTimeTableEvents(ClaimDB.getAll_ClaimTimeTableEvents_TimeTableEvent_Id(@event.Id));

            int count = 0;

            if (clist.Count > 0 && builder.Length > 0 )
                builder.Append(",");

            foreach (ClaimTimeTableEvent @cevent in clist)
            {
                List<Responsibility> resp = ResponsibilityDB.readResponsibilities(ResponsibilityDB.getUserResponsibility_By_Id(@cevent.FK_User_Responsibility));
                
                builder.Append("{");
                builder.Append("\"allDay\" : false,");
                builder.Append("\"id\" : \"" + @cevent.Id + "\",");
                builder.Append("\"userRId\" : \"" + @cevent.FK_User_Responsibility + "\",");
                builder.Append("\"parentId\" : \"" + @cevent.FK_TimeTableEvent + "\",");
                builder.Append("\"color\" :\"#FF530D\",");
                if (resp.Count > 0)
                    builder.Append("\"title\" : \""+ resp[0].Description +"\",");
                builder.Append("\"start\": " + "\"" + @cevent.Start_Date.ToString("yyyy-MM-dd HH:mm:ss") + "\",");
                builder.Append("\"end\": " + "\"" + @cevent.End_Date.ToString("yyyy-MM-dd HH:mm:ss") + "\",");
                builder.Append("\"type\" : \"created\",");
                builder.Append("\"editable\" :\"true\"");
                builder.Append("}");
                if (count < clist.Count - 1)
                    builder.Append(",");
                count++;
            }
        //}

        return builder.ToString();
    }

    public String getColor(int id)
    {
        string color = "";
        switch (id)
        {
            case 30:
                color = "#000000";
                break;
            case 32:
                color = "";
                break;
        }

        return color;
    }

    protected String getModule(int id)
    {
       Module module =  TimeTableDB._getModule(id);
       if (module != null) return module.Code;
       else return "";
    }

    protected String getDateTime(DateTime date, int day, TimeSpan span)
    {
        DateTime dateTime = new DateTime(date.Year, date.Month, date.Day + day, span.Hours, span.Minutes, span.Seconds);
        return dateTime.ToString("HH:mm:00");
    }

    public ClaimTimeTableEvent getEvent()
    {
        ClaimTimeTableEvent @event = new ClaimTimeTableEvent();
        @event.FK_TimeTableEvent = Convert.ToInt32(Request.Form["parentId"]);
        @event.FK_User_Responsibility = Convert.ToInt32(Request.Form["userRId"]);
        @event.Start_Date = Convert.ToDateTime(Request.Form["start"]);
        @event.End_Date = Convert.ToDateTime(Request.Form["end"]);
        try
        {
            @event.Id = Convert.ToInt32(Request.Form["id"]);
        }
        catch { }
        return @event;
    }

    protected int getNumberOfDays(DateTime start, DateTime end)
    {
        int num = 0;

        DayOfWeek day = start.DayOfWeek;
        DateTime date = start;

        while (date <= end)
        {
            num++;
            date = date.AddDays(7);
        }

        return num;
    }

    protected void replicateForPeriod(Claim claim, ClaimTimeTableEvent @event )
    {
        DateTime sdate = new DateTime(claim.Period_From.Year, claim.Period_From.Month, claim.Period_From.Day,
                                        @event.Start_Date.Hour, @event.Start_Date.Minute, @event.Start_Date.Second);

        DateTime edate = new DateTime(claim.Period_From.Year, claim.Period_From.Month, claim.Period_From.Day,
                                        @event.End_Date.Hour, @event.End_Date.Minute, @event.End_Date.Second);

        DayOfWeek dayOfWeekEvent = @event.Start_Date.DayOfWeek;
        DayOfWeek dayOfWeekClaim = claim.Period_From.DayOfWeek;

        if (dayOfWeekEvent != dayOfWeekClaim)
        {
            UInt32 days = (UInt32)dayOfWeekEvent - (UInt32)dayOfWeekClaim;
            sdate = sdate.AddDays(days);
            edate = edate.AddDays(days);
        }

        DateTime endDate = DateTime.Parse(claim.Period_To.ToString("MM/dd/yyyy 00:00:00"));

        while (sdate <= endDate)
        {
            @event.Start_Date = sdate;
            @event.End_Date = edate;

            if (@event.End_Date <= endDate)
            {
                try
                {
                    ClaimDB.insertClaimTimeTableEvent(@event);
                }
                catch (Exception) { }
            }

            sdate = sdate.AddDays(7);
            edate = edate.AddDays(7);
        }
    }

    public void addEvent()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
        if (claims.Count > 0)
        {
            ClaimTimeTableEvent @event = getEvent();
            @event.FK_Claim = claims[0].Id;

            int id = @event.Id;

            if (@event.Id == 0)
            {
                id = ClaimDB.insertClaimTimeTableEvent(@event);
                //Replecate events here
                replicateForPeriod(claims[0], @event);
                //int numDays = getNumberOfDays(claims[0].Period_From, claims[0].Period_To);
            }
            else
            {
                try
                {
                    bool all = false;
                    Boolean.TryParse( Request.QueryString["all"], out all);
                    if (!all)
                        ClaimDB.updateClaimTimeTableEvent(@event);
                    else
                    {
                        ClaimDB.deleteAll_ClaimTimeTableEvent(@event.Id);
                        //Replecate events here
                        ClaimDB.insertClaimTimeTableEvent(@event);
                        replicateForPeriod(claims[0], @event);                        
                    }
                }
                catch (Exception) { }
            }

            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            if (id != 0) Response.Write("{ \"status\": \"ok\", \"id\": \""+id+"\" }");
            else Response.Write("{ \"status\": \"error\" }");
            Response.End();
        }
    }

    public void removeEvent()
    {
        int eventId = 0;
        bool all = false;

        Response.Clear();
        Response.ContentType = "application/json; charset=utf-8";

        try
        {
            eventId = Convert.ToInt32(Request.QueryString["eventId"]);
            all = Convert.ToBoolean(Request.QueryString["all"]);
            if (!all)
                ClaimDB.deleteClaimTimeTableEvent(eventId);
            else
                ClaimDB.deleteAll_ClaimTimeTableEvent(eventId);

            Response.Write("{ \"status\": \"ok\" }");
        }
        catch {

            Response.Write("{ \"status\": \"error\" }");
        }

        Response.End();
    }
}