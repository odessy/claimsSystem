﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Claim.aspx.cs" Inherits="User_Claim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server">
    <ul class="breadcrumb">
        <li class="active"><asp:LinkButton ID="lbHome" PostBackUrl="~/Claim/Claim" runat="server" >Claim</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbSelection" PostBackUrl="~/Claim/TimeTable" runat="server" >Selection<span style="z-index: 99999; left: 10px; font-size: 16px;" class="showopacity glyphicon glyphicon-calendar"></span></asp:LinkButton></li>
        <li><asp:LinkButton ID="lbReview" PostBackUrl="~/Claim/Review" runat="server" >Review</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbConfirmation" runat="server" >Confirmation</asp:LinkButton></li>
    </ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div>
            <h3>Let's Get Started</h3>
            <p>Here's How Selection Works</p>
            <div class="steps clearfix">
                <div class="step">
                    <h2>Step 1 : Selection</h2>
                    <p>
                        Select the dates you worked based on
                    your timetable(s) using our easy to
                    use timetable selector.
                    </p>
                </div>
                <div class="step">
                    <h2>Step 2: Review</h2>
                    <p>
                        Review the dates you selected and 
                    the calculations for your overtime claim
                    </p>
                </div>
                <div class="step">
                    <h2>Step 3: Confirmation</h2>
                    <p>
                        Submit the form for futher review
                        and processing by the school.
                    </p>
                </div>
            </div>
            <div id="questions">
                <p>Would you like to select date from your timetable?</p>
                <asp:RadioButtonList ID="rbList" runat="server">
                    <asp:ListItem Selected="True" Value="Yes">&nbsp Yes</asp:ListItem>
                    <asp:ListItem  Value="No">&nbsp No</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>


        
        <asp:LinkButton ID="btnBack" CausesValidation="true" CssClass="btn btn-primary btn-lg"
            runat="server" Text="Back" Enabled="false" />
        <asp:LinkButton ID="btnContinue" CausesValidation="true" CssClass="btn btn-primary btn-lg"
            runat="server" Text="Continue" OnClick="btnContinue_Click" />
    </div>
</asp:Content>