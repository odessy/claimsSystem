﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_TimeTable : System.Web.UI.Page
{
    protected void Bind(int semesterId, UserAccount user)
    {
        eventRepeater.DataSource = ResponsibilityDB.getAllUserResponsibilities_ByUserId((Guid)user.userId, semesterId);
        eventRepeater.DataBind();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UserAccount user = UserAccount.getLoginUserInfo();
            List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
            if (claims.Count > 0)
            {
                Bind(claims[0].FK_Semester_Id, user);
            }
        }
    }
}