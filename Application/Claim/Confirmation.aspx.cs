﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Confirmation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
        if (claims.Count > 0)
        {
            if (claims[0].Total_Claim > 0)
            {
                if (cbConfirm.Checked)
                {
                    //Submit the claim here
                    claims[0].Status = 1; //elevate the status of the claim.
                    int updated = ClaimDB.updateClaim(claims[0]);
                    if (updated == -1)
                    {
                        FlashHelper.FlashInfo("Your claim was submitted successfully");
                        Response.Redirect("~/User/Home.aspx");
                    }
                    else
                        FlashHelper.FlashError("Unable to submit your claim at this moment.");

                }
                else
                {
                    FlashHelper.FlashError("You need to confirm your submission.");
                }
            }
            else
            {
                FlashHelper.FlashError("No claims currently available for submission.");
            }
        }
    }
}