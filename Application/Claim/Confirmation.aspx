﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Confirmation.aspx.cs" Inherits="User_Confirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server">
    <ul class="breadcrumb">
        <li><asp:LinkButton ID="lbHome" PostBackUrl="~/Claim/Claim" runat="server" >Claim</asp:LinkButton></li>
        <li><asp:LinkButton ID="lbSelection" PostBackUrl="~/Claim/TimeTable" runat="server" >Selection<span style="z-index: 99999; left: 10px; font-size: 16px;" class="showopacity glyphicon glyphicon-calendar"></span></asp:LinkButton></li>
        <li><asp:LinkButton ID="lbReview" PostBackUrl="~/Claim/Review" runat="server" >Review</asp:LinkButton></li>
        <li class="active"><asp:LinkButton ID="lbConfirmation" runat="server" >Confirmation</asp:LinkButton></li>
    </ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div style="padding-bottom:5px; line-height: 30px;" >
        <div class="about">
            The claim will be submited to relevant users on the system. You will be notified of the different stages the claim reaches.
        </div>

        <div>
            <asp:CheckBox ID="cbConfirm" Checked="false" runat="server" />
            I hereby confirm that I am about to submit a cliam for processing and I have read and agreed to the Terms and Conditions of this system.
        </div>
    </div>
    <asp:LinkButton ID="btnBack" CausesValidation="true" CssClass="btn btn-primary btn-lg"
        runat="server" Text="Back" PostBackUrl="~/Claim/Review.aspx" />
    <asp:LinkButton ID="btnConfirm" CausesValidation="true" CssClass="btn btn-primary btn-lg"
        runat="server" OnClick="btnConfirm_Click" Text="Submit Claim"  />

</asp:Content>
