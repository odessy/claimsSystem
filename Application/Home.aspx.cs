﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Context.User.Identity.IsAuthenticated == true)
        {
            HttpContext.Current.Response.Redirect("~/User/Home.aspx");
        }

        //Add recurring job to scrape website
        //String path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        //HangFireJobs.Scrape.ScrapTimeTable(System.IO.Path.GetDirectoryName(path) + "\\");
    }
}