﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Password.aspx.cs" Inherits="Password" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Password</h1></asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div class="form">
        <div class="alert alert-help prepend-top-default"> Change your password </div>
        <p>
            You must provide current password in order to change it.
        </p>
        <p>
            After a successful password update, you will be redirected to the login page where you can log in with your new password. 
        </p>
        <br />
        <div class="control-div clearfix">
            <asp:Label ID="lbCurrentPassword" runat="server" CssClass="control-label" Text="Label">Current Password</asp:Label>
            <div class="col-sm-10">
                <asp:TextBox ID="tbCurrentPassword" TextMode="Password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvCurrentPassword" runat="server"
                    ControlToValidate="tbCurrentPassword"
                    ErrorMessage="Current Password is required.">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-div clearfix">
            <asp:Label ID="lbPassword" runat="server" CssClass="control-label" Text="Label">New Password</asp:Label>
            <div class="col-sm-10">
                <asp:TextBox ID="tbPassword" TextMode="Password" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvPassword" runat="server"
                    ControlToValidate="tbPassword"
                    ErrorMessage="Password is required.">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-div clearfix">
            <asp:Label ID="lbConfirmPassword" runat="server" CssClass="control-label" Text="Label">Confirm Password</asp:Label>
            <div class="col-sm-10">
                <asp:TextBox ID="tbConfirmPassword" TextMode="Password" runat="server"></asp:TextBox>
                <asp:CompareValidator ForeColor="Red" ID="cVConfirmPassword" runat="server"
                    ControlToValidate="tbConfirmPassword"
                    ControlToCompare="tbPassword"
                    Type="String"
                    ToolTip="Password must be the same"
                    ErrorMessage="Confirm Password does not match New Password.">
                </asp:CompareValidator>
                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvConfirmPassword" runat="server"
                    ControlToValidate="tbConfirmPassword"
                    CssClass="absolute"
                    ErrorMessage="Confirm Password is required.">
                </asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group clearfix">
            <div class="col-sm-offset-2 col-sm-10 ">
                <asp:Button ID="btnSubmit" CausesValidation="true" CssClass="button large green"
                    runat="server" OnClick="btnSubmit_Click" Text="Change Password" />
            </div>
        </div>
    </div>
</asp:Content>