﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Home : System.Web.UI.Page
{
    protected void BindList()
    {
        UserAccount user = UserAccount.getLoginUserInfo();
        System.Data.DataSet set = ClaimDB.getClaims_ByUserId((Guid)user.userId);
        lvClaim.DataSource = set;
        lvClaim.DataBind();

        System.Data.DataSet sSet = TimeTableDB.getAllSemester();
        List<Semester> list = TimeTableDB.readSemester(sSet);
        ddlSemester.DataSource = list;
        ddlSemester.DataTextField = "DisplayField";
        ddlSemester.DataValueField = "Id";
        ddlSemester.DataBind();

        if (list.Count > 0)
        {
            BindPeriod(list[0]);
        }
    }

    protected void BindPeriod(Semester semester)
    {
        ddlPeriod.Items.Clear();
        ddlPeriod.Items.Add(new ListItem("All", semester.DisplayField));
        foreach( Period period in TimeTableDB.generatePeriods(semester))
        {
            ddlPeriod.Items.Add(new ListItem(period.DisplayField, period.DisplayField));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if ( UserAccount.isUserElevated() )
        {
            Response.Redirect("~/Admin/Home.aspx");
        }

        BindList();
    }

    protected void DataPager1_PreRender(object sender, EventArgs e)
    {
        lvClaim.DataBind();
    }

    protected void btnCreateNew_Click(object sender, EventArgs e)
    {

        try
        {
            DateTime from, to;

            if (!cbManual.Checked)
            {
                String[] periods = ddlPeriod.SelectedValue.Split('-');
                from = DateTime.ParseExact(periods[0].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                to = DateTime.ParseExact(periods[1].Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                from = DateTime.ParseExact(tbFrom.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                to = DateTime.ParseExact(tbTo.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            
            }
            //find semester from period
            List<Semester> semesters = TimeTableDB.readSemester(TimeTableDB.getSemester_ByPeriod(from, to));

            if (semesters.Count > 0)
            {

                UserAccount user = UserAccount.getLoginUserInfo();
                //Check if an open claim is in system if not create one
                List<Claim> claims = ClaimDB.readClaims(ClaimDB.getAll_Claim_ByStatus(0, (Guid)user.userId));
                if (claims.Count == 0)
                {
                    //Create claim with open status
                    Claim claim = new Claim();
                    claim.FK_Semester_Id = semesters[0].Id;
                    claim.FK_UserId = user.userId;
                    claim.Status = 0;
                    claim.Rate_Per_hour = 0;
                    claim.Total_Claim = 0;
                    claim.Total_Minutes = 0;
                    claim.Total_Taxable_Claim = 0;
                    claim.Total_Allowances = 0;
                    claim.Period_From = from;
                    claim.Period_To = to;

                    ClaimDB.insertClaim(claim);

                    FlashHelper.FlashInfo("New Claim Created");

                    BindList();
                }
                else
                    FlashHelper.FlashWarning("Please complete the open cliam before creating another.");
            }
            else
                FlashHelper.FlashWarning("Could not identify a valid semester. Unable to proceed.");
        }
        catch (Exception)
        {
            FlashHelper.FlashError("Valid date periods are required in the format mm/dd/yyyy.");
        }
    }

    protected string getSemesterDate(string semesterId)
    {
        try
        {
            int Id = Convert.ToInt32(semesterId);
            Semester semester = TimeTableDB._getSemester(Id);
            if (semester != null) return semester.Start_Date.ToString("yyyy-MM-dd") + " to " + semester.End_Date.ToString("yyyy-MM-dd");
        }
        catch (Exception) { };

        return "unknown";
    }

    protected void ViewButton_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        Response.Redirect("/Claim/ClaimView?claimId="+btn.CommandArgument);
    }

    protected void EditButton_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        Response.Redirect("/Claim/Claim");
    }

    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(ddlSemester.SelectedValue);
        Semester semester = TimeTableDB._getSemester(id);
        if (semester != null) BindPeriod(semester);
    }
    
    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;

            int claimId = Convert.ToInt32(btn.CommandArgument);
            Claim claim = ClaimDB.readClaims(ClaimDB.getClaim_ById(claimId))[0];
            if (claim.Status == 0 && 
                (Guid)UserAccount.getLoginUserInfo().userId == (Guid)claim.FK_UserId)
            {
                ClaimDB.deleteClaim(claimId);
                FlashHelper.FlashWarning("Claim " + claimId + " deleted from our system");
                BindList();
                return;
            }
        }
        catch (Exception)
        {
        }
        
        FlashHelper.FlashWarning("Unable to delete claim ");
    }
    
}