﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Profile.aspx.cs" Inherits="User_Profile"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Profile Settings</h1></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:FormView ID="fvUsers" CssClass="form clearfix" runat="server" AllowPaging="false"
        OnItemUpdating="fvUsers_ItemUpdating">
        <EditItemTemplate>
            <div class="alert alert-help prepend-top-default"> This information will appear on your profile. </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbFirstName" CssClass="control-label" runat="server" Text="First Name"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbFirstName" runat="server" Text='<%# Bind("firstName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvFirstName" runat="server"
                        ControlToValidate="tbFirstName"
                        ErrorMessage="First Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbLastName" CssClass="control-label" runat="server" Text="Last Name"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbLastName" runat="server" Text='<%# Bind("lastName") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvLastName" runat="server"
                        ControlToValidate="tbLastName"
                        ErrorMessage="Last Name is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbIdNumber" CssClass="control-label" runat="server" Text="ID Number"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbIdNumber" Enabled="false" runat="server" Text='<%# Bind("idNumber") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="RequiredFieldValidator1" runat="server"
                        ControlToValidate="tbIdNumber"
                        ErrorMessage="ID Number is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbEmail" CssClass="control-label" runat="server" Text="Email Address"></asp:Label>
                <div class="col-sm-10">
                    <asp:TextBox ID="tbEmail" Enabled="false" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ForeColor="Red" ID="rfvEmail" runat="server"
                        ControlToValidate="tbEmail"
                        ErrorMessage="Email is required.">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-div clearfix">
                <asp:Label ID="lbRole" CssClass="control-label" runat="server" Text="User Role(s)"></asp:Label>
                <% //select the role for the current user
                    //must be done before the dropDownList control
                    DropDownList ddl = (fvUsers.FindControl("ddlRole") as DropDownList);
                    object roles = DataBinder.Eval(fvUsers.DataItem, "role");
                    if (ddl != null && roles != null)
                    {
                        if(roles.GetType() != typeof(System.DBNull)) {
                            foreach (string role in (string [])roles)
                            {
                                ddl.Items.Add(new ListItem(role, role));
                            }
                        }
                    }
                %>
                <div class="col-sm-10">
                    <asp:DropDownList Enabled="true" ID="ddlRole" runat="server">
                    </asp:DropDownList>
                    <span style="visibility:hidden;">placeholder</span>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-sm-offset-2 col-sm-10 ">
                    <asp:Button ID="btnEditUser" CssClass="button large green" 
                        runat="server" CommandName="Update" Text="Update" />
                </div>
            </div>
        </EditItemTemplate>
        <EmptyDataTemplate>
            No user data found.
        </EmptyDataTemplate>
    </asp:FormView>
</asp:Content>

