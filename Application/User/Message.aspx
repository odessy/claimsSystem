﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Message.aspx.cs" Inherits="User_Message" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentHead" Runat="Server"><h1 class="page-title">Message: Inbox</h1></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <%--<div class="form">
        <div class="alert alert-help prepend-top-default">Send and recieve messages </div>
        <br />
    </div>--%>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#messageModal">Compose</button><br /><br />
    <div>
        <div class="alert alert-help prepend-top-default">Message Inbox </div>

        <asp:ListView ID="lvMessage" runat="server" DataKeyNames="Id"  
            OnItemCommand="lvMessage_ItemCommand"
            >
            <LayoutTemplate>
                <table id="itemPlaceholderContainer" class="gridTable" runat="server" border="0" style="">

                    <tr id="Tr2" runat="server" style="">
                        <th id="Th1" runat="server">Subject</th>
                        <th id="Th2" runat="server">From</th>
                        <th id="Th3" runat="server">Message</th>
                        <th id="Th4" runat="server">Date</th>
                    </tr>
                    <tr id="itemPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr style="">
                    <td>
                        <asp:Label ID="lbSubject" runat="server" Text='<%# Eval("Subject").ToString() %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbClaimId" runat="server" Text='<%# getUsername(Eval("FK_From_UserId").ToString())%>' />
                    </td>
                    <td>
                        <asp:Label ID="lbMessage" runat="server" Text='<%# Eval("Message").ToString() %>' />
                    </td>
                    <td>
                        <asp:Label ID="lbDate" runat="server" Text='<%# Eval("Recieved_Date").ToString() %>' />
                    </td>
                    <td runat="server" Visible='<%# isInbox(Eval("FK_To_UserId").ToString()) %>'>
                        <asp:Label ID="lbStatusUnread" Visible='<%# Convert.ToInt32(Eval("Status")) != 0 %>' runat="server" Text="Message Read" />
                        <asp:Label ID="lbStatusRead" Visible='<%# Convert.ToInt32(Eval("Status")) == 0 %>' runat="server" style="color:#f00" Text="Messasge Unread" />
                    </td>
                     <td  runat="server">
                        <asp:LinkButton CausesValidation="false" ID="ReadButton" CssClass="btn btn-primary" runat="server"  CommandArgument='<%# Bind("Id") %>' CommandName="Read" Text="Read" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

    </div>

    <!-- Modal -->
    <div id="messageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3 class="modal-title" id="myModalLabel">Send Message</h3>
                </div>
                <div class="modal-body form">
                    <div class="form">
                        <br />
                        <div class="control-div clearfix">
                            <asp:Label ID="lbTo" runat="server" CssClass="control-label">To</asp:Label>
                            <div class="col-sm-10">
                                <!--<asp:TextBox ID="tbTo" runat="server"></asp:TextBox> -->
                                <asp:DropDownList ID="ddlUsers" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvTo" runat="server"
                                    ControlToValidate="ddlUsers"
                                      ErrorMessage="Reciever is required.">
                                </asp:RequiredFieldValidator>

                                <!--<asp:DropDownList ID="ddlUserxs" runat="server"></asp:DropDownList>-->

                            </div>
                        </div>
                        <div class="control-div clearfix">
                            <asp:Label ID="lbSubject" runat="server" CssClass="control-label">Subject</asp:Label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="tbSubject" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvSubject" runat="server"
                                    ControlToValidate="tbSubject"
                                    ErrorMessage="Subject is required.">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="control-div clearfix">
                            <asp:Label ID="lbMessage" runat="server" CssClass="control-label">Message</asp:Label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="tbMessage" Style="width: 98%; height: 200px;" TextMode="MultiLine" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvMessage" runat="server"
                                    ControlToValidate="tbMessage"
                                    CssClass="inplace"
                                    ErrorMessage="Message is required.">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" id="cancelButton" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <asp:Button ID="btnSubmit" CausesValidation="true" CssClass="btn btn-primary"
                            runat="server" OnClick="btnSubmit_Click" Text="Send Message" />
                    </div>
                </div>
            </div>
            <!--/.modal-content-->
        </div>
        <!--/.modal-dialog -->
    </div>

    <!-- Modal for Read -->
    <div id="readMessageModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="messageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <h3 class="modal-title" id="H1">Message</h3>
                </div>
                <div class="modal-body form">
                    <div class="form">
                        <br />
                        From: <asp:Label ID="lblFromInfo" runat="server"> </asp:Label><br />
                        To: <asp:Label ID="lblToInfo" runat="server">  </asp:Label><br />
                        Subject: <asp:Label ID="lblSubjectInfo" runat="server"> </asp:Label><br />
                        Message: <asp:Label ID="lblMessageInfo" runat="server" > </asp:Label><br />
                        Date: <asp:Label ID="lblDateInfo" runat="server" > </asp:Label>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" id="btnClose" data-dismiss="modal" aria-hidden="true">Close</button>
      
                    </div>
                </div>
            </div>
            <!--/.modal-content-->
        </div>
        <!--/.modal-dialog -->
    </div>

    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FooterScript" runat="server">
    <script type="text/javascript">
        function ShowModalPopUp() {
            $('#readMessageModal').modal('show');
        }
        $(document).ready(function () {
            <% if(ShowModal) { %>
            ShowModalPopUp();
            <% } %>
        });
    </script>
</asp:Content>
