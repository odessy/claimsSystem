﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Message : System.Web.UI.Page
{
    public bool ShowModal = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindList(Membership.GetAllUsers());
            BindList();

        }
    }

    protected String getUsername(String userId)
    {
        try
        {
            UserAccount user = UserAccount.getUserInfo(new Guid(userId));
            return user.firstName + " " + user.lastName;
        } catch(Exception)
        {
        }

        return "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        MessageObj obj = new MessageObj();

        obj.From_UserId = new Guid(Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString());
        obj.To_UserId = new Guid(ddlUsers.SelectedValue);
        obj.Message = tbMessage.Text;
        obj.Subject = tbSubject.Text;
        obj.Received_Date = DateTime.Now;
        obj.Status = 0;

        MessageDB.insertMessage(obj);
        BindList();
    }

    private void BindList(MembershipUserCollection membershipUserCollection)
    {
        ddlUsers.DataSource = membershipUserCollection;
        ddlUsers.DataTextField = "email";//getUsername("ProviderUserKey");
        ddlUsers.DataValueField = "ProviderUserKey";
        ddlUsers.DataBind();
    }

    protected void BindList()
    {
        lvMessage.DataSource = MessageDB.getAllMessage_ByUserId((Guid)UserAccount.getLoginUserInfo().userId);
        lvMessage.DataBind();
    }

    protected bool isInbox(string userId)
    {
        Guid user = (Guid)UserAccount.getLoginUserInfo().userId;
        return user == new Guid(userId);
    }

    protected void lvMessage_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "Read")
        {
            MessageObj message = new MessageObj();
            List<MessageObj> messages = MessageDB.readMessage( MessageDB.getAllMessage_ById(Convert.ToInt32(e.CommandArgument.ToString())));
            if (messages.Count > 0)
                message = messages[0];

            //Display message
            lblDateInfo.Text = message.Received_Date.ToString();
            lblFromInfo.Text = getUsername(Convert.ToString(message.From_UserId));
            lblToInfo.Text = getUsername(Convert.ToString(message.To_UserId));
            lblMessageInfo.Text = message.Message;
            lblSubjectInfo.Text = message.Subject; 

            if(isInbox(message.To_UserId.ToString()))
            {
                message.Status = 1;
                MessageDB.updateMessageStatus(message);
            }

            ShowModal = true;

            BindList();
        }
    }
}