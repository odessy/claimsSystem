﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //update user password if changed
        if (!String.IsNullOrEmpty(tbCurrentPassword.Text) &&
            !String.IsNullOrEmpty(tbPassword.Text))
            if (Membership.GetUser().ChangePassword(tbCurrentPassword.Text, tbPassword.Text))
            {
                FormsAuthentication.SignOut();
                FlashHelper.FlashInfo("Password updated. Please login again.");
                Response.Redirect("~/Account/Login");
            }
            else
            {
                FlashHelper.FlashError("Unable to Update Password");
            }
    }

}